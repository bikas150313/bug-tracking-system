-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 19, 2018 at 07:47 AM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `aseassignment77193461`
--
CREATE DATABASE IF NOT EXISTS `aseassignment77193461` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `aseassignment77193461`;

-- --------------------------------------------------------

--
-- Table structure for table `bug`
--

CREATE TABLE `bug` (
  `bug_id` int(11) NOT NULL,
  `bug_description` varchar(255) NOT NULL,
  `project` varchar(50) NOT NULL,
  `class` varchar(30) DEFAULT NULL,
  `method` varchar(30) DEFAULT NULL,
  `line_number` varchar(30) DEFAULT NULL,
  `code_author` varchar(30) DEFAULT NULL,
  `screenshot` longblob,
  `report_date` varchar(30) NOT NULL,
  `status` varchar(30) DEFAULT NULL,
  `reporter_id` int(11) DEFAULT NULL,
  `fix_date` varchar(30) DEFAULT NULL,
  `fixer_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bug`
--

INSERT INTO `bug` (`bug_id`, `bug_description`, `project`, `class`, `method`, `line_number`, `code_author`, `screenshot`, `report_date`, `status`, `reporter_id`, `fix_date`, `fixer_id`) VALUES
(1, 'Issue in System.', 'Bug Tracking System', '', '', '', '', 0x53797374656d2e427974655b5d, 'MAY-18-2018', 'Active', 11, NULL, NULL),
(2, 'Abc', 'abc', '', '', '', '', 0x53797374656d2e427974655b5d, 'JAN-1-2018', 'Active', 11, NULL, NULL),
(3, 'fghhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh', 'abc', '', '', '', '', 0x53797374656d2e427974655b5d, 'JAN-1-2018', 'Active', 11, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL,
  `full_name` varchar(50) DEFAULT NULL,
  `address` varchar(30) DEFAULT NULL,
  `contact_number` varchar(30) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `role` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `username`, `password`, `full_name`, `address`, `contact_number`, `email`, `role`) VALUES
(1, 'admin', 'admin', NULL, NULL, NULL, NULL, 'Admin'),
(10, 'bikas', 'bikas', 'Bikash Dumrakoti', 'Satungal', '9818469538', 'bikasdumrakoti@gmail.com', 'Developer'),
(11, 'abc', 'abc', 'MD Imran', 'Balkumari', '9876543210', 'horizonfirestrom@gmail.com', 'Tester'),
(12, 'bravo', 'bravo', 'Bravo Delta', 'No Mans Land', '0000000000', 'gmail@bravodelta.com', 'Tester'),
(13, 'roshan', 'roshan', 'Roshan Raj Thapaliya', 'Nepaltar', '9876543210', 'roshan.boy@gmail.com', 'Developer');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bug`
--
ALTER TABLE `bug`
  ADD PRIMARY KEY (`bug_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bug`
--
ALTER TABLE `bug`
  MODIFY `bug_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
