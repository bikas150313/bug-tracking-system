﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugTrackingSystem.edu.tbc77193461.model
{
    class Bug
    {
        /// <summary>
        /// declaring required variables with private access modifier
        /// </summary>
        private int bugId;
        private int userId;
        private string bugDescription;
        private string project;
        private string className;
        private string method;
        private string lineNumber;
        private string author;
        private string date;
        private string fileName;
        private string status;
        private string fixDate;
        private int fixerId;
        private int reporterId;

        /// <summary>
        /// getter and setter for bug id
        /// </summary>
        public int BugId
        {
            get
            {
                return bugId;
            }
            set
            {
                bugId = value;
            }
        }

        /// <summary>
        /// getter and setter for user id
        /// </summary>
        public int UserId
        {
            get
            {
                return userId;
            }
            set
            {
                userId = value;
            }
        }

        /// <summary>
        /// getter and setter for bug description
        /// </summary>
        public string BugDescription
        {
            get
            {
                return bugDescription;
            }
            set
            {
                bugDescription = value;
            }
        }

        /// <summary>
        /// getter and setter for project
        /// </summary>
        public string Project
        {
            get
            {
                return project;
            }
            set
            {
                project = value;
            }
        }

        /// <summary>
        /// getter and setter for class name
        /// </summary>
        public string ClassName
        {
            get
            {
                return className;
            }
            set
            {
                className = value;
            }
        }

        /// <summary>
        /// getter and setter for method
        /// </summary>
        public string Method
        {
            get
            {
                return method;
            }
            set
            {
                method = value;
            }
        }

        /// <summary>
        /// getter and setter for line number
        /// </summary>
        public string LineNumber
        {
            get
            {
                return lineNumber;
            }
            set
            {
                lineNumber = value;
            }
        }

        /// <summary>
        /// getter and setter for author
        /// </summary>
        public string Author
        {
            get
            {
                return author;
            }
            set
            {
                author = value;
            }
        }

        /// <summary>
        /// getter and setter for date
        /// </summary>
        public string Date
        {
            get
            {
                return date;
            }
            set
            {
                date = value;
            }
        }

        /// <summary>
        /// getter and setter for file name
        /// </summary>
        public string FileName
        {
            get
            {
                return fileName;
            }
            set
            {
                fileName = value;
            }
        }

        /// <summary>
        /// getter and setter for status
        /// </summary>
        public string Status
        {
            get
            {
                return status;
            }
            set
            {
                status = value;
            }
        }

        /// <summary>
        /// getter and setter for fix date
        /// </summary>
        public string FixDate
        {
            get
            {
                return fixDate;
            }
            set
            {
                fixDate = value;
            }
        }

        /// <summary>
        /// getter and setter for fixer id
        /// </summary>
        public int FixerId
        {
            get
            {
                return fixerId;
            }
            set
            {
                fixerId = value;
            }
        }

        /// <summary>
        /// getter and setter for reporter id
        /// </summary>
        public int ReporterId
        {
            get
            {
                return reporterId;
            }
            set
            {
                reporterId = value;
            }
        }
    }
}
