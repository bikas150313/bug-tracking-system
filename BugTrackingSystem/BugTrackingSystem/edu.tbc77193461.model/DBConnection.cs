﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BugTrackingSystem.edu.tbc77193461.model
{
    class DBConnection
    {

        /// <summary>
        /// variable to get database connection
        /// </summary>
        private static MySqlConnection databaseConnection = null;

        public static MySqlConnection GetConnection()
        {
            //if connection object is null then,
            //get database connection
            if (databaseConnection==null)
            {
                //calling connection method
                ConnectToDB();
            }
            return databaseConnection;
        }

        private static void ConnectToDB()
        {
            try
            {
                //connection string
                string mySqlConnectionString = "datasource=localhost;username=root;password=;database=aseassignment77193461;SSLmode=none";
                //getting connection
                databaseConnection = new MySqlConnection(mySqlConnectionString);
            } catch (MySqlException ex)
            {
                MessageBox.Show("Sorry, unable to connect to database!");
            }
        }
    }
}
