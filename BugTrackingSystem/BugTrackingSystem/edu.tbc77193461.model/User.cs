﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugTrackingSystem.edu.tbc77193461.model
{
    class User
    {

        /// <summary>
        /// declaring required variables with private access modifier
        /// </summary>
        private int userId;
        private string username;
        private string password;
        private string fullName;
        private string address;
        private string contactNo;
        private string email;
        private string role;

        /// <summary>
        /// getter and setter for user id
        /// </summary>
        public int UserId
        {
            get
            {
                return userId;
            }
            set
            {
                userId = value;
            }
        }

        /// <summary>
        /// getter and setter for username
        /// </summary>
        public string Username
        {
            get
            {
                return username;
            }
            set
            {
                username = value;
            }
        }

        /// <summary>
        /// getter and setter for password
        /// </summary>
        public string Password
        {
            get
            {
                return password;
            }
            set
            {
                password = value;
            }
        }

        /// <summary>
        /// getter and setter for full name
        /// </summary>
        public string FullName
        {
            get
            {
                return fullName;
            }
            set
            {
                fullName = value;
            }
        }

        /// <summary>
        /// getter and setter for address
        /// </summary>
        public string Address
        {
            get
            {
                return address;
            }
            set
            {
                address = value;
            }
        }

        /// <summary>
        /// getter and setter for contact number
        /// </summary>
        public string ContactNo
        {
            get
            {
                return contactNo;
            }
            set
            {
                contactNo = value;
            }
        }

        /// <summary>
        /// getter and setter for email
        /// </summary>
        public string Email
        {
            get
            {
                return email;
            }
            set
            {
                email = value;
            }
        }

        /// <summary>
        /// getter and setter for role
        /// </summary>
        public string Role
        {
            get
            {
                return role;
            }
            set
            {
                role = value;
            }
        }
    }
}
