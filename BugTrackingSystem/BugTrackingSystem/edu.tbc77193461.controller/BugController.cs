﻿using BugTrackingSystem.edu.tbc77193461.model;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BugTrackingSystem.edu.tbc77193461.controller
{
    class BugController
    {
        /// <summary>
        /// declaring required variables
        /// </summary>
        private MySqlConnection databaseConnection = null;
        private MySqlCommand commandDatabase;
        FileStream fileStream;
        BinaryReader binaryReader;

        public BugController()
        {
            //getting database connection
            if (databaseConnection == null)
            {
                databaseConnection = DBConnection.GetConnection();
            }
        }

        /// <summary>
        /// insert bug report in database
        /// </summary>
        /// <param name="bug"></param>
        /// <returns></returns>
        public Boolean ReportBug(Bug bug)
        {
            Boolean isReported = false;
            //read image file as byte
            byte[] ImageData;
            fileStream = new FileStream(bug.FileName, FileMode.Open, FileAccess.Read);
            binaryReader = new BinaryReader(fileStream);
            ImageData = binaryReader.ReadBytes((int)fileStream.Length);
            binaryReader.Close();
            fileStream.Close();
            string query = "insert into bug (bug_description, project, class, method, " +
                "line_number, code_author, screenshot, report_date, status, reporter_id)" +
                "values ('" + bug.BugDescription + "', '" + bug.Project + "', " +
                "'" + bug.ClassName + "', '" + bug.Method + "', '" + bug.LineNumber + "',"
                + "'" + bug.Author + "', @screenshot, '" + bug.Date + "'," +
                "'" + bug.Status + "', '" + bug.UserId + "');";
            try
            {
                databaseConnection.Open();
                commandDatabase = new MySqlCommand(query, databaseConnection);
                commandDatabase.Parameters.AddWithValue("screenshot", ImageData);
                int affectedRows = commandDatabase.ExecuteNonQuery();
                if (affectedRows > 0)
                {
                    isReported = true;
                }
                databaseConnection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Query Error : " + ex.Message);
            }
            return isReported;
        }

        /// <summary>
        /// load all personal bugs
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public DataTable LoadMyBugs(int userId)
        {
            DataTable dataTable = new DataTable();
            string query = "select * from bug where reporter_id='" + userId + "';";
            try
            {
                databaseConnection.Open();
                commandDatabase = new MySqlCommand(query, databaseConnection);
                MySqlDataAdapter dataAdapter = new MySqlDataAdapter();
                dataAdapter.SelectCommand = commandDatabase;
                dataAdapter.Fill(dataTable);
                databaseConnection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Query Error : " + ex.Message);
            }
            return dataTable;
        }

        /// <summary>
        /// retrieve bug fixer full name
        /// </summary>
        /// <param name="bug"></param>
        /// <returns></returns>
        public string RetrieveFixer(Bug bug)
        {
            string fixer = "";
            string query = "select full_name from user where user_id='" + bug.FixerId + "';";
            try
            {
                databaseConnection.Open();
                commandDatabase = new MySqlCommand(query, databaseConnection);
                MySqlDataReader reader = commandDatabase.ExecuteReader();
                while (reader.Read())
                {
                    fixer = reader["full_name"].ToString();
                }
                databaseConnection.Close();
            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Error : " + ex);
            }
            return fixer;
        }

        /// <summary>
        /// update bug detail
        /// </summary>
        /// <param name="bug"></param>
        /// <returns></returns>
        public Boolean UpdateBug(Bug bug)
        {
            Boolean isUpdated = false;
            string query = "update bug set bug_description='" + bug.BugDescription + "', " +
                "project='" + bug.Project + "'," +
                "class='" + bug.ClassName + "', method='" + bug.Method + "', " +
                "line_number='" + bug.LineNumber + "', code_author='" + bug.Author + "' where bug_id='" + bug.BugId + "';";
            try
            {
                databaseConnection.Open();
                commandDatabase = new MySqlCommand(query, databaseConnection);
                int updatedRow = commandDatabase.ExecuteNonQuery();
                if (updatedRow > 0)
                {
                    isUpdated = true;
                }
                databaseConnection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Query Error : " + ex.Message);
            }
            return isUpdated;
        }

        /// <summary>
        /// load all active bugs
        /// </summary>
        /// <returns></returns>
        public DataTable LoadActiveBugs()
        {
            DataTable dataTable = new DataTable();
            string query = "select * from bug where status=@status;";
            try
            {
                databaseConnection.Open();
                commandDatabase = new MySqlCommand(query, databaseConnection);
                commandDatabase.Parameters.AddWithValue("status", "Active");
                MySqlDataAdapter dataAdapter = new MySqlDataAdapter();
                dataAdapter.SelectCommand = commandDatabase;
                dataAdapter.Fill(dataTable);
                databaseConnection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Query Error : " + ex.Message);
            }
            return dataTable;
        }

        /// <summary>
        /// retrieve bug reporter full name
        /// </summary>
        /// <param name="bug"></param>
        /// <returns></returns>
        public string RetrieveReporter(Bug bug)
        {
            string reporter = "";
            string query = "select full_name from user where user_id='" + bug.ReporterId + "';";
            try
            {
                databaseConnection.Open();
                commandDatabase = new MySqlCommand(query, databaseConnection);
                MySqlDataReader reader = commandDatabase.ExecuteReader();
                while (reader.Read())
                {
                    reporter = reader["full_name"].ToString();
                }
                databaseConnection.Close();
            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Error : " + ex);
            }
            return reporter;
        }

        /// <summary>
        /// update bug status as fixing
        /// </summary>
        /// <param name="bug"></param>
        /// <returns></returns>
        public Boolean UpdateBugStatusFixing(Bug bug)
        {
            Boolean isUpdated = false;
            string query = "update bug set status='" + bug.Status + "', fixer_id='" + bug.FixerId + "' " +
                "where bug_id='" + bug.BugId + "';";
            try
            {
                databaseConnection.Open();
                commandDatabase = new MySqlCommand(query, databaseConnection);
                int updatedRow = commandDatabase.ExecuteNonQuery();
                if (updatedRow > 0)
                {
                    isUpdated = true;
                }
                databaseConnection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Query Error : " + ex.Message);
            }
            return isUpdated;
        }

        /// <summary>
        /// update bug status as fixed
        /// </summary>
        /// <param name="bug"></param>
        /// <returns></returns>
        public Boolean UpdateBugStatusFixed(Bug bug)
        {
            Boolean isUpdated = false;
            string query = "update bug set status='" + bug.Status + "', fix_date='" + bug.FixDate + "', " +
                "fixer_id='" + bug.FixerId + "' " + "where bug_id='" + bug.BugId + "';";
            try
            {
                databaseConnection.Open();
                commandDatabase = new MySqlCommand(query, databaseConnection);
                int updatedRow = commandDatabase.ExecuteNonQuery();
                if (updatedRow > 0)
                {
                    isUpdated = true;
                }
                databaseConnection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Query Error : " + ex.Message);
            }
            return isUpdated;
        }

        /// <summary>
        /// load bugs to fix
        /// </summary>
        /// <param name="bug"></param>
        /// <returns></returns>
        public DataTable LoadToFixBugs(Bug bug)
        {
            DataTable dataTable = new DataTable();
            string query = "select * from bug where status=@status and fixer_id='" + bug.FixerId + "';";
            try
            {
                databaseConnection.Open();
                commandDatabase = new MySqlCommand(query, databaseConnection);
                commandDatabase.Parameters.AddWithValue("status", "Fixing");
                MySqlDataAdapter dataAdapter = new MySqlDataAdapter();
                dataAdapter.SelectCommand = commandDatabase;
                dataAdapter.Fill(dataTable);
                databaseConnection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Query Error : " + ex.Message);
            }
            return dataTable;
        }
    }
}
