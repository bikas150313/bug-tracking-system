﻿using BugTrackingSystem.edu.tbc77193461.model;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BugTrackingSystem.edu.tbc77193461.controller
{
    class UserController
    {
        /// <summary>
        /// declaring required variables
        /// </summary>
        public string fullName, username, address, contactNo, email, role;
        private MySqlConnection databaseConnection = null;
        private MySqlCommand commandDatabase;

        public UserController()
        {
            //getting database connection
            if (databaseConnection==null)
            {
                databaseConnection = DBConnection.GetConnection();
            }
        }

        /// <summary>
        /// authenticate user
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public Boolean Authenticate(User user)
        {
            Boolean isValid = false;
            string query = "select * from user where username='"+user.Username+"' " +
                "and password='"+user.Password+"';";
            try
            {
                databaseConnection.Open();
                commandDatabase = new MySqlCommand(query, databaseConnection);
                MySqlDataReader reader = commandDatabase.ExecuteReader();
                while (reader.Read())
                {
                    isValid = true;
                }
                databaseConnection.Close();
            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Error : " +ex);
            }
            return isValid;
        }

        /// <summary>
        /// update user password
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public Boolean UpdatePassword(User user)
        {
            Boolean isUpdated = false;
            string query = "update user set password='" + user.Password + "' where username='" + user.Username + "';";
            try
            {
                databaseConnection.Open();
                commandDatabase = new MySqlCommand(query, databaseConnection);
                int updatedRow = commandDatabase.ExecuteNonQuery();
                if (updatedRow > 0)
                {
                    isUpdated = true;
                }
                databaseConnection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Query Error : " +ex.Message); 
            }
            return isUpdated;
        }

        /// <summary>
        /// check username if exists
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public Boolean CheckUsername(User user)
        {
            Boolean userFound = false;
            string query = "select * from user where username='"+user.Username+"';";
            try
            {
                databaseConnection.Open();
                commandDatabase = new MySqlCommand(query, databaseConnection);
                MySqlDataReader reader = commandDatabase.ExecuteReader();
                while (reader.Read())
                {
                    userFound = true;
                }
                databaseConnection.Close();
            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Error : " + ex);
            }
            return userFound;
        }

        /// <summary>
        /// check email if exists
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public Boolean CheckEmail(User user)
        {
            Boolean emailFound = false;
            string query = "select * from user where email='" + user.Email + "';";
            try
            {
                databaseConnection.Open();
                commandDatabase = new MySqlCommand(query, databaseConnection);
                MySqlDataReader reader = commandDatabase.ExecuteReader();
                while (reader.Read())
                {
                    emailFound = true;
                }
                databaseConnection.Close();
            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Error : " + ex);
            }
            return emailFound;
        }

        /// <summary>
        /// add new user
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public Boolean AddUser(User user)
        {
            Boolean userAdded = false;
            string query = "insert into user (username, password, full_name, address, contact_number, email, role)" +
                "values ('" + user.Username + "', '" + user.Password + "', '" + user.FullName + "', '" + user.Address + "', '" + user.ContactNo + "'," +
                "'" + user.Email + "', '" + user.Role + "');";
            try
            {
                databaseConnection.Open();
                commandDatabase = new MySqlCommand(query, databaseConnection);
                int affectedRows = commandDatabase.ExecuteNonQuery();
                if (affectedRows > 0)
                {
                    userAdded = true;
                }
                databaseConnection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Query Error : " + ex.Message);
            }
            return userAdded;
        }

        /// <summary>
        /// load all users
        /// </summary>
        /// <returns></returns>
        public DataTable LoadAllUsers()
        {
            DataTable dataTable = new DataTable();
            string query = "select * from user;";
            try
            {
                databaseConnection.Open();
                commandDatabase = new MySqlCommand(query, databaseConnection);
                MySqlDataAdapter dataAdapter = new MySqlDataAdapter();
                dataAdapter.SelectCommand = commandDatabase;
                dataAdapter.Fill(dataTable);
                databaseConnection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Query Error : " + ex.Message);
            }
            return dataTable;
        }

        /// <summary>
        /// delete particular user
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public Boolean DeleteUser(User user)
        {
            Boolean userDeleted = false;
            string query = "delete from user where user_id='" + user.UserId + "' and username='" + user.Username + "';";
            try
            {
                databaseConnection.Open();
                commandDatabase = new MySqlCommand(query, databaseConnection);
                int affectedRows = commandDatabase.ExecuteNonQuery();
                if (affectedRows > 0)
                {
                    userDeleted = true;
                }
                databaseConnection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Query Error : " + ex.Message);
            }
            return userDeleted;
        }

        /// <summary>
        /// update user details
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public Boolean UpdateUser(User user)
        {
            Boolean isUpdated = false;
            string query = "update user set full_name='" + user.FullName + "', " +
                "address='" + user.Address + "'," +
                "contact_number='" + user.ContactNo + "', email='" + user.Email + "', " +
                "role='" + user.Role + "' where user_id='" + user.UserId + "' and " +
                "username='" + user.Username + "';";
            try
            {
                databaseConnection.Open();
                commandDatabase = new MySqlCommand(query, databaseConnection);
                int updatedRow = commandDatabase.ExecuteNonQuery();
                if (updatedRow > 0)
                {
                    isUpdated = true;
                }
                databaseConnection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Query Error : " + ex.Message);
            }
            return isUpdated;
        }

        /// <summary>
        /// retrieve user role
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public string RetrieveRole(User user)
        {
            string role = "";
            string query = "select role from user where username='" + user.Username + "' and password='" + user.Password + "';";
            try
            {
                databaseConnection.Open();
                commandDatabase = new MySqlCommand(query, databaseConnection);
                MySqlDataReader reader = commandDatabase.ExecuteReader();
                while (reader.Read())
                {
                    role = reader["role"].ToString();
                }
                databaseConnection.Close();
            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Error : " + ex);
            }
            return role;
        }

        /// <summary>
        /// retrieve user id based on username and password
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public int RetrieveUserId(User user)
        {
            int userId = 0;
            string query = "select user_id from user where username='" + user.Username + "' and password='" + user.Password + "';";
            try
            {
                databaseConnection.Open();
                commandDatabase = new MySqlCommand(query, databaseConnection);
                MySqlDataReader reader = commandDatabase.ExecuteReader();
                while (reader.Read())
                {
                    userId = Convert.ToInt32(reader["user_id"]);
                }
                databaseConnection.Close();
            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Error : " + ex);
            }
            return userId;
        }

        /// <summary>
        /// retrieve user data 
        /// </summary>
        /// <param name="user"></param>
        public void RetrieveUserData(User user)
        {
            string query = "select * from user where user_id='" + user.UserId + "';";
            try
            {
                databaseConnection.Open();
                commandDatabase = new MySqlCommand(query, databaseConnection);
                MySqlDataReader reader = commandDatabase.ExecuteReader();
                while (reader.Read())
                {
                    fullName = reader["full_name"].ToString();
                    username = reader["username"].ToString();
                    address = reader["address"].ToString();
                    contactNo = reader["contact_number"].ToString();
                    email = reader["email"].ToString();
                    role = reader["role"].ToString();
                }
                databaseConnection.Close();
            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Error : " + ex);
            }
        }
    }
}
