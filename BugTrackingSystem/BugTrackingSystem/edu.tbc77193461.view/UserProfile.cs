﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BugTrackingSystem.edu.tbc77193461.view
{
    public partial class UserProfile : Form
    {
        public UserProfile(string fullName, string username, string address,
            string contactNo, string email, string role)
        {
            InitializeComponent();
            //initilaizing form components
            txt_fullName.Text = fullName;
            txt_userName.Text = username;
            txt_address.Text = address;
            txt_contactNo.Text = contactNo;
            txt_email.Text = email;
            txt_role.Text = role;
        }

        /// <summary>
        /// event triggered on ok button click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_ok_Click(object sender, EventArgs e)
        {
            Dispose();
        }
    }
}
