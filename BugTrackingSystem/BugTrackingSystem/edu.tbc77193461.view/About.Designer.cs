﻿namespace BugTrackingSystem.edu.tbc77193461.view
{
    partial class About
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(About));
            this.pan_about = new System.Windows.Forms.Panel();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.lbl_bugTrackingSystem = new System.Windows.Forms.Label();
            this.lbl_version = new System.Windows.Forms.Label();
            this.pan_about.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // pan_about
            // 
            this.pan_about.BackColor = System.Drawing.Color.Black;
            this.pan_about.Controls.Add(this.lbl_version);
            this.pan_about.Controls.Add(this.lbl_bugTrackingSystem);
            this.pan_about.Controls.Add(this.pictureBox);
            this.pan_about.Location = new System.Drawing.Point(1, 1);
            this.pan_about.Name = "pan_about";
            this.pan_about.Size = new System.Drawing.Size(582, 266);
            this.pan_about.TabIndex = 0;
            // 
            // pictureBox
            // 
            this.pictureBox.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox.Image")));
            this.pictureBox.Location = new System.Drawing.Point(0, 0);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(339, 266);
            this.pictureBox.TabIndex = 1;
            this.pictureBox.TabStop = false;
            // 
            // lbl_bugTrackingSystem
            // 
            this.lbl_bugTrackingSystem.AutoSize = true;
            this.lbl_bugTrackingSystem.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_bugTrackingSystem.ForeColor = System.Drawing.Color.White;
            this.lbl_bugTrackingSystem.Location = new System.Drawing.Point(344, 73);
            this.lbl_bugTrackingSystem.Name = "lbl_bugTrackingSystem";
            this.lbl_bugTrackingSystem.Size = new System.Drawing.Size(235, 25);
            this.lbl_bugTrackingSystem.TabIndex = 2;
            this.lbl_bugTrackingSystem.Text = "Bug Tracking System";
            // 
            // lbl_version
            // 
            this.lbl_version.AutoSize = true;
            this.lbl_version.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_version.ForeColor = System.Drawing.Color.White;
            this.lbl_version.Location = new System.Drawing.Point(345, 107);
            this.lbl_version.Name = "lbl_version";
            this.lbl_version.Size = new System.Drawing.Size(89, 20);
            this.lbl_version.TabIndex = 3;
            this.lbl_version.Text = "Version 1.0";
            // 
            // About
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(583, 267);
            this.Controls.Add(this.pan_about);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "About";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "About";
            this.pan_about.ResumeLayout(false);
            this.pan_about.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pan_about;
        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.Label lbl_bugTrackingSystem;
        private System.Windows.Forms.Label lbl_version;
    }
}