﻿using BugTrackingSystem.edu.tbc77193461.controller;
using BugTrackingSystem.edu.tbc77193461.model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BugTrackingSystem
{
    public partial class ChangePassword : Form
    {

        /// <summary>
        /// declaring required variable
        /// </summary>
        private string username;

        public ChangePassword(string username)
        {
            this.username = username;
            InitializeComponent();
            txt_userName.Text = this.username;
        }

        /// <summary>
        /// event triggered on cancel button click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_cancel_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        /// <summary>
        /// event triggered on update button click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_update_Click(object sender, EventArgs e)
        {
            string username = txt_userName.Text;
            string currentPassword = txt_currentPassword.Text;
            string newPassword = txt_newPassword.Text;
            string confirmPassword = txt_confirmPassword.Text;
            if (!string.IsNullOrEmpty(currentPassword) && !string.IsNullOrEmpty(newPassword) && !string.IsNullOrEmpty(confirmPassword))
            {
                if (newPassword==confirmPassword)
                {
                    User user = new User();
                    user.Username = username;
                    user.Password = currentPassword;
                    UserController userController = new UserController();
                    Boolean valid = userController.Authenticate(user);
                    if (valid)
                    {
                        user.Password = newPassword;
                        Boolean updated = userController.UpdatePassword(user);
                        if (updated)
                        {
                            MessageBox.Show("Password Updated!");
                            txt_currentPassword.Text = "";
                            txt_newPassword.Text = "";
                            txt_confirmPassword.Text = "";
                            txt_currentPassword.Focus();
                        }
                        else
                        {
                            MessageBox.Show("Unable to update password!");
                        }
                    }
                    else
                    {
                        MessageBox.Show("The current password you entered is incorrect!");
                        txt_currentPassword.Focus();
                    }
                }
                else
                {
                    MessageBox.Show("The new password and confirm password fields does not match!");
                    txt_confirmPassword.Focus();
                }
            }
            else
            {
                MessageBox.Show("Please input all the fields!");
            }
        }
    }
}
