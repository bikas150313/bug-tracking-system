﻿using BugTrackingSystem.edu.tbc77193461.controller;
using BugTrackingSystem.edu.tbc77193461.model;
using BugTrackingSystem.edu.tbc77193461.view;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BugTrackingSystem
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        /// <summary>
        /// event triggered on cancel button click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_cancel_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        /// <summary>
        /// event triggered on login button click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_login_Click(object sender, EventArgs e)
        {
            string username = txt_userName.Text;
            string password = txt_password.Text;
            if (!string.IsNullOrEmpty(username))
            {
                if (!string.IsNullOrEmpty(password))
                {
                    User user = new User();
                    user.Username = username;
                    user.Password = password;
                    UserController userController = new UserController();
                    Boolean valid = userController.Authenticate(user);
                    if (valid)
                    {
                        string role = userController.RetrieveRole(user);
                        int userId = userController.RetrieveUserId(user);
                        //opening required user dashboard
                        if (role=="Admin")
                        {
                            this.Hide();
                            AdminDashboard adminDashboard = new AdminDashboard(username);
                            adminDashboard.ShowDialog();
                            this.Close();
                        }
                        else if (role == "Developer")
                        {
                            this.Hide();
                            DeveloperDashboard developerDashboard = new DeveloperDashboard(username, userId);
                            developerDashboard.ShowDialog();
                            this.Close();
                        }
                        else
                        {
                            this.Hide();
                            TesterDashboard testerDashboard = new TesterDashboard(username, userId);
                            testerDashboard.ShowDialog();
                            this.Close();
                        }
                    }
                    else
                    {
                        MessageBox.Show("Invalid username or password!");
                        txt_password.Text = "";
                        txt_password.Focus();
                    }
                }
                else
                {
                    MessageBox.Show("Please enter your password!");
                    txt_password.Focus();
                }
            }
            else
            {
                MessageBox.Show("Please enter your username!");
                txt_userName.Focus();
            }
        }
    }
}
