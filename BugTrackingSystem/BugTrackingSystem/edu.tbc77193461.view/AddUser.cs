﻿using BugTrackingSystem.edu.tbc77193461.controller;
using BugTrackingSystem.edu.tbc77193461.model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BugTrackingSystem
{
    public partial class AddUser : Form
    {
        public AddUser()
        {
            InitializeComponent();
        }

        /// <summary>
        /// event triggered on cancel button click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_cancel_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        /// <summary>
        /// event triggered on add button click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_add_Click(object sender, EventArgs e)
        {
            string fullName = txt_fullName.Text;
            string username = txt_userName.Text;
            string address = txt_address.Text;
            string contactNo = txt_contactNo.Text;
            string email = txt_email.Text;
            string role = combo_role.Text;
            string password = txt_password.Text;
            string confirmPassword = txt_confirmPassword.Text;
            //validation
            if (!string.IsNullOrEmpty(fullName) && !string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(address)
                && !string.IsNullOrEmpty(contactNo) && !string.IsNullOrEmpty(email) && !string.IsNullOrEmpty(role)
                && !string.IsNullOrEmpty(password) && !string.IsNullOrEmpty(confirmPassword))
            {
                if (password==confirmPassword)
                {
                    User user = new User();
                    user.Username = username;
                    UserController userController = new UserController();
                    Boolean usernameFound = userController.CheckUsername(user);
                    if (!usernameFound)
                    {
                        user.Email = email;
                        Boolean emailFound = userController.CheckEmail(user);
                        if (!emailFound)
                        {
                            user.FullName = fullName;
                            user.Address = address;
                            user.ContactNo = contactNo;
                            user.Role = role;
                            user.Password = password;
                            Boolean userAdded = userController.AddUser(user);
                            if (userAdded)
                            {
                                MessageBox.Show("User successfully added!");
                                txt_fullName.Text = "";
                                txt_userName.Text = "";
                                txt_address.Text = "";
                                txt_contactNo.Text = "";
                                txt_email.Text = "";
                                combo_role.Text = "";
                                txt_password.Text = "";
                                txt_confirmPassword.Text = "";
                                txt_fullName.Focus();
                            }
                            else
                            {
                                MessageBox.Show("Unable to add user!");
                            }
                        }
                        else
                        {
                            MessageBox.Show("This email already exists!");
                            txt_email.Focus();
                        }
                    }
                    else
                    {
                        MessageBox.Show("This username is already taken! Enter a different username!");
                        txt_userName.Focus();
                    }
                }
                else
                {
                    MessageBox.Show("Your password fields does not match!");
                    txt_confirmPassword.Focus();
                }
            }
            else
            {
                MessageBox.Show("Please input all the fields!");
            }
        }
    }
}
