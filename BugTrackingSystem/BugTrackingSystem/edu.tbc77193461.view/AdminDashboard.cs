﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BugTrackingSystem.edu.tbc77193461.view
{
    public partial class AdminDashboard : Form
    {

        /// <summary>
        /// declaring required variable
        /// </summary>
        private string username;

        public AdminDashboard()
        {

        }

        public AdminDashboard(string username)
        {
            this.username = username;
            this.WindowState = FormWindowState.Maximized;
            InitializeComponent();
        }

        /// <summary>
        /// event triggered on logout menu click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void menuItem_logout_Click(object sender, EventArgs e)
        {
            this.Hide();
            Login login = new Login();
            login.ShowDialog();
            this.Close();
        }

        /// <summary>
        /// event triggered on change password menu click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void menuItem_changePassword_Click(object sender, EventArgs e)
        {
            ChangePassword changePassword = new ChangePassword(username);
            changePassword.ShowDialog();
        }

        /// <summary>
        /// event triggered on add user menu click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void menuItem_addUser_Click(object sender, EventArgs e)
        {
            AddUser addUser = new AddUser();
            addUser.ShowDialog();
        }

        /// <summary>
        /// event triggered on view user menu click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void menuItem_viewUser_Click(object sender, EventArgs e)
        {
            ViewAllUsers allUsers = new ViewAllUsers();
            allUsers.ShowDialog();
        }
        
        /// <summary>
        /// opens edit user form
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="username"></param>
        /// <param name="fullName"></param>
        /// <param name="address"></param>
        /// <param name="contactNo"></param>
        /// <param name="email"></param>
        /// <param name="role"></param>
        public void OpenEditUserForm(int userId, string username, string fullName, string address,
            string contactNo, string email, string role)
        {
            EditUser editUser = new EditUser(userId, username, fullName, address, contactNo,
                email, role);
            editUser.ShowDialog();
        }

        /// <summary>
        /// event triggered on connect to bitbucket menu click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void menuItem_connectToBitbucket_Click(object sender, EventArgs e)
        {
            //url for bitbucket
            string url = "https://bitbucket.org/account/signin/";
            //execute url
            System.Diagnostics.Process.Start(url);
        }

        /// <summary>
        /// event triggered on about menu click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void menuItem_about_Click(object sender, EventArgs e)
        {
            About about = new About();
            about.ShowDialog();
        }
    }
}
