﻿using BugTrackingSystem.edu.tbc77193461.controller;
using BugTrackingSystem.edu.tbc77193461.model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BugTrackingSystem.edu.tbc77193461.view
{
    public partial class ViewAllUsers : Form
    {

        public ViewAllUsers()
        {
            InitializeComponent();
            LoadAllUsers();
        }

        /// <summary>
        /// load all users
        /// </summary>
        public void LoadAllUsers()
        {
            UserController userController = new UserController();
            DataTable dataTable = userController.LoadAllUsers();
            //populating datagridview with data source 
            dataGridView.DataSource = dataTable;
            //setting columns visibility
            dataGridView.Columns["password"].Visible = false;
            //removing row
            dataGridView.Rows.Remove(dataGridView.Rows[0]);
            //editing columns header
            dataGridView.Columns["user_id"].HeaderText = "User ID";
            dataGridView.Columns["username"].HeaderText = "Username";
            dataGridView.Columns["full_name"].HeaderText = "Full Name";
            dataGridView.Columns["address"].HeaderText = "Address";
            dataGridView.Columns["contact_number"].HeaderText = "Contact Number";
            dataGridView.Columns["email"].HeaderText = "Email";
            dataGridView.Columns["role"].HeaderText = "Role";
            //setting user edit role
            dataGridView.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView.AllowUserToAddRows = false;
            dataGridView.AllowUserToDeleteRows = false;
            dataGridView.MultiSelect = false;
            dataGridView.AllowUserToResizeRows = false;
            dataGridView.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            
        }

        /// <summary>
        /// event triggered on edit button click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_edit_Click(object sender, EventArgs e)
        {
            if (dataGridView.SelectedRows.Count != 0)
            {
                //getting cell values
                int userId = Convert.ToInt32(dataGridView.SelectedRows[0].Cells[0].Value);
                string username = dataGridView.SelectedRows[0].Cells[1].Value.ToString();
                string fullName = dataGridView.SelectedRows[0].Cells[3].Value.ToString();
                string address = dataGridView.SelectedRows[0].Cells[4].Value.ToString();
                string contactNo = dataGridView.SelectedRows[0].Cells[5].Value.ToString();
                string email = dataGridView.SelectedRows[0].Cells[6].Value.ToString();
                string role = dataGridView.SelectedRows[0].Cells[7].Value.ToString();
                AdminDashboard dashboard = new AdminDashboard();
                dashboard.OpenEditUserForm(userId, username, fullName, address, contactNo,
                    email, role);
            }
            else
            {
                MessageBox.Show("No row selected! Please select a row!");
            }
        }

        /// <summary>
        /// event triggered on delete button click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_delete_Click(object sender, EventArgs e)
        {
            if (dataGridView.SelectedRows.Count!=0)
            {
                DialogResult result = MessageBox.Show("Do you want to delete selected row?", "Delete User?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {
                    int userId = Convert.ToInt32(dataGridView.SelectedRows[0].Cells[0].Value);
                    string username = dataGridView.SelectedRows[0].Cells[1].Value.ToString();
                    User user = new User();
                    user.UserId = userId;
                    user.Username = username;
                    UserController userController = new UserController();
                    Boolean userDeleted = userController.DeleteUser(user);
                    if (userDeleted)
                    {
                        MessageBox.Show("User Deleted!");
                    }
                    else
                    {
                        MessageBox.Show("Unable to delete user!");
                    }
                }
            }
            else
            {
                MessageBox.Show("No row selected! Please select a row!");
            }
        }

        /// <summary>
        /// event triggered on refresh button click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_refresh_Click(object sender, EventArgs e)
        {
            //refresh datagridview
            dataGridView.Refresh();
            LoadAllUsers();
        }
    }
}
