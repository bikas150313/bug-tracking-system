﻿namespace BugTrackingSystem.edu.tbc77193461.view
{
    partial class UserProfile
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserProfile));
            this.pan_addUser = new System.Windows.Forms.Panel();
            this.btn_ok = new System.Windows.Forms.Button();
            this.lbl_role = new System.Windows.Forms.Label();
            this.txt_email = new System.Windows.Forms.TextBox();
            this.lbl_email = new System.Windows.Forms.Label();
            this.txt_contactNo = new System.Windows.Forms.TextBox();
            this.lbl_contactNo = new System.Windows.Forms.Label();
            this.txt_address = new System.Windows.Forms.TextBox();
            this.lbl_address = new System.Windows.Forms.Label();
            this.txt_userName = new System.Windows.Forms.TextBox();
            this.lbl_userName = new System.Windows.Forms.Label();
            this.txt_fullName = new System.Windows.Forms.TextBox();
            this.lbl_fullName = new System.Windows.Forms.Label();
            this.lbl_yourProfile = new System.Windows.Forms.Label();
            this.txt_role = new System.Windows.Forms.TextBox();
            this.pan_addUser.SuspendLayout();
            this.SuspendLayout();
            // 
            // pan_addUser
            // 
            this.pan_addUser.BackColor = System.Drawing.Color.Black;
            this.pan_addUser.Controls.Add(this.txt_role);
            this.pan_addUser.Controls.Add(this.btn_ok);
            this.pan_addUser.Controls.Add(this.lbl_role);
            this.pan_addUser.Controls.Add(this.txt_email);
            this.pan_addUser.Controls.Add(this.lbl_email);
            this.pan_addUser.Controls.Add(this.txt_contactNo);
            this.pan_addUser.Controls.Add(this.lbl_contactNo);
            this.pan_addUser.Controls.Add(this.txt_address);
            this.pan_addUser.Controls.Add(this.lbl_address);
            this.pan_addUser.Controls.Add(this.txt_userName);
            this.pan_addUser.Controls.Add(this.lbl_userName);
            this.pan_addUser.Controls.Add(this.txt_fullName);
            this.pan_addUser.Controls.Add(this.lbl_fullName);
            this.pan_addUser.Controls.Add(this.lbl_yourProfile);
            this.pan_addUser.Location = new System.Drawing.Point(0, -1);
            this.pan_addUser.Name = "pan_addUser";
            this.pan_addUser.Size = new System.Drawing.Size(824, 436);
            this.pan_addUser.TabIndex = 1;
            // 
            // btn_ok
            // 
            this.btn_ok.Font = new System.Drawing.Font("Algerian", 15.75F);
            this.btn_ok.ForeColor = System.Drawing.Color.Green;
            this.btn_ok.Image = ((System.Drawing.Image)(resources.GetObject("btn_ok.Image")));
            this.btn_ok.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_ok.Location = new System.Drawing.Point(383, 388);
            this.btn_ok.Name = "btn_ok";
            this.btn_ok.Size = new System.Drawing.Size(69, 36);
            this.btn_ok.TabIndex = 22;
            this.btn_ok.Text = "Ok";
            this.btn_ok.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_ok.UseVisualStyleBackColor = true;
            this.btn_ok.Click += new System.EventHandler(this.btn_ok_Click);
            // 
            // lbl_role
            // 
            this.lbl_role.AutoSize = true;
            this.lbl_role.Font = new System.Drawing.Font("Hobo Std", 21.75F, System.Drawing.FontStyle.Bold);
            this.lbl_role.ForeColor = System.Drawing.Color.MintCream;
            this.lbl_role.Location = new System.Drawing.Point(12, 308);
            this.lbl_role.Name = "lbl_role";
            this.lbl_role.Size = new System.Drawing.Size(97, 41);
            this.lbl_role.TabIndex = 16;
            this.lbl_role.Text = "Role :";
            // 
            // txt_email
            // 
            this.txt_email.Enabled = false;
            this.txt_email.Font = new System.Drawing.Font("Consolas", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_email.ForeColor = System.Drawing.Color.Blue;
            this.txt_email.Location = new System.Drawing.Point(332, 263);
            this.txt_email.Name = "txt_email";
            this.txt_email.ReadOnly = true;
            this.txt_email.Size = new System.Drawing.Size(480, 32);
            this.txt_email.TabIndex = 15;
            // 
            // lbl_email
            // 
            this.lbl_email.AutoSize = true;
            this.lbl_email.Font = new System.Drawing.Font("Hobo Std", 21.75F, System.Drawing.FontStyle.Bold);
            this.lbl_email.ForeColor = System.Drawing.Color.MintCream;
            this.lbl_email.Location = new System.Drawing.Point(12, 254);
            this.lbl_email.Name = "lbl_email";
            this.lbl_email.Size = new System.Drawing.Size(111, 41);
            this.lbl_email.TabIndex = 14;
            this.lbl_email.Text = "Email :";
            // 
            // txt_contactNo
            // 
            this.txt_contactNo.Enabled = false;
            this.txt_contactNo.Font = new System.Drawing.Font("Consolas", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_contactNo.ForeColor = System.Drawing.Color.Blue;
            this.txt_contactNo.Location = new System.Drawing.Point(332, 210);
            this.txt_contactNo.Name = "txt_contactNo";
            this.txt_contactNo.ReadOnly = true;
            this.txt_contactNo.Size = new System.Drawing.Size(480, 32);
            this.txt_contactNo.TabIndex = 13;
            // 
            // lbl_contactNo
            // 
            this.lbl_contactNo.AutoSize = true;
            this.lbl_contactNo.Font = new System.Drawing.Font("Hobo Std", 21.75F, System.Drawing.FontStyle.Bold);
            this.lbl_contactNo.ForeColor = System.Drawing.Color.MintCream;
            this.lbl_contactNo.Location = new System.Drawing.Point(12, 201);
            this.lbl_contactNo.Name = "lbl_contactNo";
            this.lbl_contactNo.Size = new System.Drawing.Size(259, 41);
            this.lbl_contactNo.TabIndex = 12;
            this.lbl_contactNo.Text = "Contact Number :";
            // 
            // txt_address
            // 
            this.txt_address.Enabled = false;
            this.txt_address.Font = new System.Drawing.Font("Consolas", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_address.ForeColor = System.Drawing.Color.Blue;
            this.txt_address.Location = new System.Drawing.Point(332, 159);
            this.txt_address.Name = "txt_address";
            this.txt_address.ReadOnly = true;
            this.txt_address.Size = new System.Drawing.Size(480, 32);
            this.txt_address.TabIndex = 11;
            // 
            // lbl_address
            // 
            this.lbl_address.AutoSize = true;
            this.lbl_address.Font = new System.Drawing.Font("Hobo Std", 21.75F, System.Drawing.FontStyle.Bold);
            this.lbl_address.ForeColor = System.Drawing.Color.MintCream;
            this.lbl_address.Location = new System.Drawing.Point(12, 150);
            this.lbl_address.Name = "lbl_address";
            this.lbl_address.Size = new System.Drawing.Size(152, 41);
            this.lbl_address.TabIndex = 10;
            this.lbl_address.Text = "Address :";
            // 
            // txt_userName
            // 
            this.txt_userName.Enabled = false;
            this.txt_userName.Font = new System.Drawing.Font("Consolas", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_userName.ForeColor = System.Drawing.Color.Blue;
            this.txt_userName.Location = new System.Drawing.Point(332, 104);
            this.txt_userName.Name = "txt_userName";
            this.txt_userName.ReadOnly = true;
            this.txt_userName.Size = new System.Drawing.Size(480, 32);
            this.txt_userName.TabIndex = 9;
            // 
            // lbl_userName
            // 
            this.lbl_userName.AutoSize = true;
            this.lbl_userName.Font = new System.Drawing.Font("Hobo Std", 21.75F, System.Drawing.FontStyle.Bold);
            this.lbl_userName.ForeColor = System.Drawing.Color.MintCream;
            this.lbl_userName.Location = new System.Drawing.Point(12, 94);
            this.lbl_userName.Name = "lbl_userName";
            this.lbl_userName.Size = new System.Drawing.Size(176, 41);
            this.lbl_userName.TabIndex = 8;
            this.lbl_userName.Text = "Username :";
            // 
            // txt_fullName
            // 
            this.txt_fullName.Enabled = false;
            this.txt_fullName.Font = new System.Drawing.Font("Consolas", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_fullName.ForeColor = System.Drawing.Color.Blue;
            this.txt_fullName.Location = new System.Drawing.Point(332, 53);
            this.txt_fullName.Name = "txt_fullName";
            this.txt_fullName.ReadOnly = true;
            this.txt_fullName.Size = new System.Drawing.Size(480, 32);
            this.txt_fullName.TabIndex = 7;
            // 
            // lbl_fullName
            // 
            this.lbl_fullName.AutoSize = true;
            this.lbl_fullName.Font = new System.Drawing.Font("Hobo Std", 21.75F, System.Drawing.FontStyle.Bold);
            this.lbl_fullName.ForeColor = System.Drawing.Color.MintCream;
            this.lbl_fullName.Location = new System.Drawing.Point(12, 44);
            this.lbl_fullName.Name = "lbl_fullName";
            this.lbl_fullName.Size = new System.Drawing.Size(173, 41);
            this.lbl_fullName.TabIndex = 6;
            this.lbl_fullName.Text = "Full Name :";
            // 
            // lbl_yourProfile
            // 
            this.lbl_yourProfile.AutoSize = true;
            this.lbl_yourProfile.Font = new System.Drawing.Font("Cooper Black", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_yourProfile.ForeColor = System.Drawing.Color.Aqua;
            this.lbl_yourProfile.Location = new System.Drawing.Point(316, 10);
            this.lbl_yourProfile.Name = "lbl_yourProfile";
            this.lbl_yourProfile.Size = new System.Drawing.Size(198, 34);
            this.lbl_yourProfile.TabIndex = 5;
            this.lbl_yourProfile.Text = "Your Profile";
            // 
            // txt_role
            // 
            this.txt_role.Enabled = false;
            this.txt_role.Font = new System.Drawing.Font("Consolas", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_role.ForeColor = System.Drawing.Color.Blue;
            this.txt_role.Location = new System.Drawing.Point(332, 317);
            this.txt_role.Name = "txt_role";
            this.txt_role.ReadOnly = true;
            this.txt_role.Size = new System.Drawing.Size(480, 32);
            this.txt_role.TabIndex = 24;
            // 
            // UserProfile
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(824, 435);
            this.Controls.Add(this.pan_addUser);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "UserProfile";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "User Profile";
            this.pan_addUser.ResumeLayout(false);
            this.pan_addUser.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pan_addUser;
        private System.Windows.Forms.Button btn_ok;
        private System.Windows.Forms.Label lbl_role;
        private System.Windows.Forms.TextBox txt_email;
        private System.Windows.Forms.Label lbl_email;
        private System.Windows.Forms.TextBox txt_contactNo;
        private System.Windows.Forms.Label lbl_contactNo;
        private System.Windows.Forms.TextBox txt_address;
        private System.Windows.Forms.Label lbl_address;
        private System.Windows.Forms.TextBox txt_userName;
        private System.Windows.Forms.Label lbl_userName;
        private System.Windows.Forms.TextBox txt_fullName;
        private System.Windows.Forms.Label lbl_fullName;
        private System.Windows.Forms.Label lbl_yourProfile;
        private System.Windows.Forms.TextBox txt_role;
    }
}