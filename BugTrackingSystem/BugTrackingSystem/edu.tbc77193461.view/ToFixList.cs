﻿using BugTrackingSystem.edu.tbc77193461.controller;
using BugTrackingSystem.edu.tbc77193461.model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BugTrackingSystem.edu.tbc77193461.view
{
    public partial class ToFixList : Form
    {

        /// <summary>
        /// declaring required variables
        /// </summary>
        private int userId;
        BugController bugController;

        public ToFixList(int userId)
        {
            this.userId = userId;
            InitializeComponent();
            LoadToFixBugs();
        }

        /// <summary>
        /// load bugs to fix
        /// </summary>
        public void LoadToFixBugs()
        {
            Bug bug = new Bug();
            bug.FixerId = userId;
            bugController = new BugController();
            DataTable dataTable = bugController.LoadToFixBugs(bug);
            //populating datagridvies with data source
            dataGridView.DataSource = dataTable;
            //setting visibility for table columns
            dataGridView.Columns["screenshot"].Visible = false;
            dataGridView.Columns["reporter_id"].Visible = false;
            dataGridView.Columns["fix_date"].Visible = false;
            dataGridView.Columns["fixer_id"].Visible = false;
            //editing column header
            dataGridView.Columns["bug_id"].HeaderText = "Bug ID";
            dataGridView.Columns["bug_description"].HeaderText = "Bug Description";
            dataGridView.Columns["project"].HeaderText = "Project";
            dataGridView.Columns["class"].HeaderText = "Class";
            dataGridView.Columns["method"].HeaderText = "Method";
            dataGridView.Columns["line_number"].HeaderText = "Line Number";
            dataGridView.Columns["code_author"].HeaderText = "Code Author";
            dataGridView.Columns["report_date"].HeaderText = "Report Date";
            dataGridView.Columns["status"].HeaderText = "Status";
            //setting user edit role
            dataGridView.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView.AllowUserToAddRows = false;
            dataGridView.AllowUserToDeleteRows = false;
            dataGridView.MultiSelect = false;
            dataGridView.AllowUserToResizeRows = false;
            dataGridView.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing;
        }

        /// <summary>
        /// event triggered on refresh button click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_refresh_Click(object sender, EventArgs e)
        {
            //refresh datagridview
            dataGridView.Refresh();
            LoadToFixBugs();
        }

        /// <summary>
        /// event triggered on view and update details button click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_viewAndUpdateDetails_Click(object sender, EventArgs e)
        {
            MemoryStream memoryStream;
            if (dataGridView.SelectedRows.Count != 0)
            {
                //getting cell values
                int bugId = Convert.ToInt32(dataGridView.SelectedRows[0].Cells[0].Value.ToString());
                string bugDescription = dataGridView.SelectedRows[0].Cells[1].Value.ToString();
                string project = dataGridView.SelectedRows[0].Cells[2].Value.ToString();
                string className = CheckNull(dataGridView.SelectedRows[0].Cells[3].Value.ToString());
                string method = CheckNull(dataGridView.SelectedRows[0].Cells[4].Value.ToString());
                string lineNo = CheckNull(dataGridView.SelectedRows[0].Cells[5].Value.ToString());
                string author = CheckNull(dataGridView.SelectedRows[0].Cells[6].Value.ToString());
                byte[] screenshot = (byte[])dataGridView.SelectedRows[0].Cells[7].Value;
                string reportDate = dataGridView.SelectedRows[0].Cells[8].Value.ToString();
                int reporterId = Convert.ToInt32(dataGridView.SelectedRows[0].Cells[10].Value.ToString());
                memoryStream = new MemoryStream(screenshot);
                Bug bug = new Bug();
                bug.ReporterId = reporterId;
                string reporter = bugController.RetrieveReporter(bug);
                DeveloperDashboard dashboard = new DeveloperDashboard();
                dashboard.OpenToFixBugDetailForm(userId, bugId, bugDescription, project, className, method, lineNo,
                    author, memoryStream, reportDate, reporter);
            }
            else
            {
                MessageBox.Show("No row selected! Please select a row!");
            }
        }

        /// <summary>
        /// checking null status
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private string CheckNull(string value)
        {
            string defaultValue = "Not Specified";
            if (!string.IsNullOrEmpty(value))
            {
                defaultValue = value;
            }
            return defaultValue;
        }
    }
}
