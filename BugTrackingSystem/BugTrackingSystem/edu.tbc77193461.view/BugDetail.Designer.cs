﻿namespace BugTrackingSystem.edu.tbc77193461.view
{
    partial class BugDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BugDetail));
            this.pan_bugDetail = new System.Windows.Forms.Panel();
            this.btn_cancel = new System.Windows.Forms.Button();
            this.btn_update = new System.Windows.Forms.Button();
            this.txt_fixedDate = new System.Windows.Forms.TextBox();
            this.lbl_fixedDate = new System.Windows.Forms.Label();
            this.txt_markedAs = new System.Windows.Forms.TextBox();
            this.lbl_markedAs = new System.Windows.Forms.Label();
            this.txt_associateDeveloper = new System.Windows.Forms.TextBox();
            this.lbl_associateDeveloper = new System.Windows.Forms.Label();
            this.txt_reportedDate = new System.Windows.Forms.TextBox();
            this.lbl_reportedDate = new System.Windows.Forms.Label();
            this.txt_status = new System.Windows.Forms.TextBox();
            this.lbl_status = new System.Windows.Forms.Label();
            this.txt_codeAuthor = new System.Windows.Forms.TextBox();
            this.lbl_codeAuthor = new System.Windows.Forms.Label();
            this.txt_lineNumber = new System.Windows.Forms.TextBox();
            this.lbl_lineNumber = new System.Windows.Forms.Label();
            this.txt_method = new System.Windows.Forms.TextBox();
            this.lbl_method = new System.Windows.Forms.Label();
            this.txt_class = new System.Windows.Forms.TextBox();
            this.lbl_class = new System.Windows.Forms.Label();
            this.txt_project = new System.Windows.Forms.TextBox();
            this.lbl_project = new System.Windows.Forms.Label();
            this.lbl_bugDescription = new System.Windows.Forms.Label();
            this.lbl_bug = new System.Windows.Forms.Label();
            this.pan_bug = new System.Windows.Forms.Panel();
            this.pictureBox_bug = new System.Windows.Forms.PictureBox();
            this.txt_bugDescription = new System.Windows.Forms.RichTextBox();
            this.pan_bugDetail.SuspendLayout();
            this.pan_bug.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_bug)).BeginInit();
            this.SuspendLayout();
            // 
            // pan_bugDetail
            // 
            this.pan_bugDetail.AutoScroll = true;
            this.pan_bugDetail.BackColor = System.Drawing.Color.Black;
            this.pan_bugDetail.Controls.Add(this.txt_bugDescription);
            this.pan_bugDetail.Controls.Add(this.btn_cancel);
            this.pan_bugDetail.Controls.Add(this.btn_update);
            this.pan_bugDetail.Controls.Add(this.txt_fixedDate);
            this.pan_bugDetail.Controls.Add(this.lbl_fixedDate);
            this.pan_bugDetail.Controls.Add(this.txt_markedAs);
            this.pan_bugDetail.Controls.Add(this.lbl_markedAs);
            this.pan_bugDetail.Controls.Add(this.txt_associateDeveloper);
            this.pan_bugDetail.Controls.Add(this.lbl_associateDeveloper);
            this.pan_bugDetail.Controls.Add(this.txt_reportedDate);
            this.pan_bugDetail.Controls.Add(this.lbl_reportedDate);
            this.pan_bugDetail.Controls.Add(this.txt_status);
            this.pan_bugDetail.Controls.Add(this.lbl_status);
            this.pan_bugDetail.Controls.Add(this.txt_codeAuthor);
            this.pan_bugDetail.Controls.Add(this.lbl_codeAuthor);
            this.pan_bugDetail.Controls.Add(this.txt_lineNumber);
            this.pan_bugDetail.Controls.Add(this.lbl_lineNumber);
            this.pan_bugDetail.Controls.Add(this.txt_method);
            this.pan_bugDetail.Controls.Add(this.lbl_method);
            this.pan_bugDetail.Controls.Add(this.txt_class);
            this.pan_bugDetail.Controls.Add(this.lbl_class);
            this.pan_bugDetail.Controls.Add(this.txt_project);
            this.pan_bugDetail.Controls.Add(this.lbl_project);
            this.pan_bugDetail.Controls.Add(this.lbl_bugDescription);
            this.pan_bugDetail.Controls.Add(this.lbl_bug);
            this.pan_bugDetail.Controls.Add(this.pan_bug);
            this.pan_bugDetail.Location = new System.Drawing.Point(0, 0);
            this.pan_bugDetail.Name = "pan_bugDetail";
            this.pan_bugDetail.Size = new System.Drawing.Size(1339, 629);
            this.pan_bugDetail.TabIndex = 0;
            // 
            // btn_cancel
            // 
            this.btn_cancel.Font = new System.Drawing.Font("Algerian", 15.75F);
            this.btn_cancel.ForeColor = System.Drawing.Color.Red;
            this.btn_cancel.Image = ((System.Drawing.Image)(resources.GetObject("btn_cancel.Image")));
            this.btn_cancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_cancel.Location = new System.Drawing.Point(736, 581);
            this.btn_cancel.Name = "btn_cancel";
            this.btn_cancel.Size = new System.Drawing.Size(118, 36);
            this.btn_cancel.TabIndex = 39;
            this.btn_cancel.Text = "Cancel";
            this.btn_cancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_cancel.UseVisualStyleBackColor = true;
            this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
            // 
            // btn_update
            // 
            this.btn_update.Font = new System.Drawing.Font("Algerian", 15.75F);
            this.btn_update.ForeColor = System.Drawing.Color.Green;
            this.btn_update.Image = ((System.Drawing.Image)(resources.GetObject("btn_update.Image")));
            this.btn_update.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_update.Location = new System.Drawing.Point(461, 581);
            this.btn_update.Name = "btn_update";
            this.btn_update.Size = new System.Drawing.Size(122, 36);
            this.btn_update.TabIndex = 38;
            this.btn_update.Text = "Update";
            this.btn_update.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_update.UseVisualStyleBackColor = true;
            this.btn_update.Click += new System.EventHandler(this.btn_update_Click);
            // 
            // txt_fixedDate
            // 
            this.txt_fixedDate.Font = new System.Drawing.Font("Consolas", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_fixedDate.ForeColor = System.Drawing.Color.Blue;
            this.txt_fixedDate.Location = new System.Drawing.Point(995, 348);
            this.txt_fixedDate.Name = "txt_fixedDate";
            this.txt_fixedDate.ReadOnly = true;
            this.txt_fixedDate.Size = new System.Drawing.Size(312, 32);
            this.txt_fixedDate.TabIndex = 37;
            // 
            // lbl_fixedDate
            // 
            this.lbl_fixedDate.AutoSize = true;
            this.lbl_fixedDate.Font = new System.Drawing.Font("Hobo Std", 21.75F, System.Drawing.FontStyle.Bold);
            this.lbl_fixedDate.ForeColor = System.Drawing.Color.MintCream;
            this.lbl_fixedDate.Location = new System.Drawing.Point(988, 295);
            this.lbl_fixedDate.Name = "lbl_fixedDate";
            this.lbl_fixedDate.Size = new System.Drawing.Size(182, 41);
            this.lbl_fixedDate.TabIndex = 36;
            this.lbl_fixedDate.Text = "Fixed Date :";
            // 
            // txt_markedAs
            // 
            this.txt_markedAs.Font = new System.Drawing.Font("Consolas", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_markedAs.ForeColor = System.Drawing.Color.Blue;
            this.txt_markedAs.Location = new System.Drawing.Point(995, 245);
            this.txt_markedAs.Name = "txt_markedAs";
            this.txt_markedAs.ReadOnly = true;
            this.txt_markedAs.Size = new System.Drawing.Size(312, 32);
            this.txt_markedAs.TabIndex = 35;
            // 
            // lbl_markedAs
            // 
            this.lbl_markedAs.AutoSize = true;
            this.lbl_markedAs.Font = new System.Drawing.Font("Hobo Std", 21.75F, System.Drawing.FontStyle.Bold);
            this.lbl_markedAs.ForeColor = System.Drawing.Color.MintCream;
            this.lbl_markedAs.Location = new System.Drawing.Point(988, 184);
            this.lbl_markedAs.Name = "lbl_markedAs";
            this.lbl_markedAs.Size = new System.Drawing.Size(186, 41);
            this.lbl_markedAs.TabIndex = 34;
            this.lbl_markedAs.Text = "Marked As :";
            // 
            // txt_associateDeveloper
            // 
            this.txt_associateDeveloper.Font = new System.Drawing.Font("Consolas", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_associateDeveloper.ForeColor = System.Drawing.Color.Blue;
            this.txt_associateDeveloper.Location = new System.Drawing.Point(995, 113);
            this.txt_associateDeveloper.Name = "txt_associateDeveloper";
            this.txt_associateDeveloper.ReadOnly = true;
            this.txt_associateDeveloper.Size = new System.Drawing.Size(312, 32);
            this.txt_associateDeveloper.TabIndex = 33;
            // 
            // lbl_associateDeveloper
            // 
            this.lbl_associateDeveloper.AutoSize = true;
            this.lbl_associateDeveloper.Font = new System.Drawing.Font("Hobo Std", 21.75F, System.Drawing.FontStyle.Bold);
            this.lbl_associateDeveloper.ForeColor = System.Drawing.Color.MintCream;
            this.lbl_associateDeveloper.Location = new System.Drawing.Point(988, 39);
            this.lbl_associateDeveloper.Name = "lbl_associateDeveloper";
            this.lbl_associateDeveloper.Size = new System.Drawing.Size(319, 41);
            this.lbl_associateDeveloper.TabIndex = 32;
            this.lbl_associateDeveloper.Text = "Associate Developer :";
            // 
            // txt_reportedDate
            // 
            this.txt_reportedDate.Font = new System.Drawing.Font("Consolas", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_reportedDate.ForeColor = System.Drawing.Color.Blue;
            this.txt_reportedDate.Location = new System.Drawing.Point(667, 511);
            this.txt_reportedDate.Name = "txt_reportedDate";
            this.txt_reportedDate.ReadOnly = true;
            this.txt_reportedDate.Size = new System.Drawing.Size(292, 32);
            this.txt_reportedDate.TabIndex = 31;
            // 
            // lbl_reportedDate
            // 
            this.lbl_reportedDate.AutoSize = true;
            this.lbl_reportedDate.Font = new System.Drawing.Font("Hobo Std", 21.75F, System.Drawing.FontStyle.Bold);
            this.lbl_reportedDate.ForeColor = System.Drawing.Color.MintCream;
            this.lbl_reportedDate.Location = new System.Drawing.Point(405, 502);
            this.lbl_reportedDate.Name = "lbl_reportedDate";
            this.lbl_reportedDate.Size = new System.Drawing.Size(235, 41);
            this.lbl_reportedDate.TabIndex = 30;
            this.lbl_reportedDate.Text = "Reported Date :";
            // 
            // txt_status
            // 
            this.txt_status.Font = new System.Drawing.Font("Consolas", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_status.ForeColor = System.Drawing.Color.Blue;
            this.txt_status.Location = new System.Drawing.Point(667, 454);
            this.txt_status.Name = "txt_status";
            this.txt_status.ReadOnly = true;
            this.txt_status.Size = new System.Drawing.Size(292, 32);
            this.txt_status.TabIndex = 29;
            // 
            // lbl_status
            // 
            this.lbl_status.AutoSize = true;
            this.lbl_status.Font = new System.Drawing.Font("Hobo Std", 21.75F, System.Drawing.FontStyle.Bold);
            this.lbl_status.ForeColor = System.Drawing.Color.MintCream;
            this.lbl_status.Location = new System.Drawing.Point(405, 445);
            this.lbl_status.Name = "lbl_status";
            this.lbl_status.Size = new System.Drawing.Size(127, 41);
            this.lbl_status.TabIndex = 28;
            this.lbl_status.Text = "Status :";
            // 
            // txt_codeAuthor
            // 
            this.txt_codeAuthor.Font = new System.Drawing.Font("Consolas", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_codeAuthor.ForeColor = System.Drawing.Color.Black;
            this.txt_codeAuthor.Location = new System.Drawing.Point(667, 398);
            this.txt_codeAuthor.Name = "txt_codeAuthor";
            this.txt_codeAuthor.Size = new System.Drawing.Size(292, 32);
            this.txt_codeAuthor.TabIndex = 27;
            // 
            // lbl_codeAuthor
            // 
            this.lbl_codeAuthor.AutoSize = true;
            this.lbl_codeAuthor.Font = new System.Drawing.Font("Hobo Std", 21.75F, System.Drawing.FontStyle.Bold);
            this.lbl_codeAuthor.ForeColor = System.Drawing.Color.MintCream;
            this.lbl_codeAuthor.Location = new System.Drawing.Point(405, 389);
            this.lbl_codeAuthor.Name = "lbl_codeAuthor";
            this.lbl_codeAuthor.Size = new System.Drawing.Size(210, 41);
            this.lbl_codeAuthor.TabIndex = 26;
            this.lbl_codeAuthor.Text = "Code Author :";
            // 
            // txt_lineNumber
            // 
            this.txt_lineNumber.Font = new System.Drawing.Font("Consolas", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_lineNumber.ForeColor = System.Drawing.Color.Black;
            this.txt_lineNumber.Location = new System.Drawing.Point(667, 347);
            this.txt_lineNumber.Name = "txt_lineNumber";
            this.txt_lineNumber.Size = new System.Drawing.Size(292, 32);
            this.txt_lineNumber.TabIndex = 25;
            // 
            // lbl_lineNumber
            // 
            this.lbl_lineNumber.AutoSize = true;
            this.lbl_lineNumber.Font = new System.Drawing.Font("Hobo Std", 21.75F, System.Drawing.FontStyle.Bold);
            this.lbl_lineNumber.ForeColor = System.Drawing.Color.MintCream;
            this.lbl_lineNumber.Location = new System.Drawing.Point(405, 338);
            this.lbl_lineNumber.Name = "lbl_lineNumber";
            this.lbl_lineNumber.Size = new System.Drawing.Size(211, 41);
            this.lbl_lineNumber.TabIndex = 24;
            this.lbl_lineNumber.Text = "Line Number :";
            // 
            // txt_method
            // 
            this.txt_method.Font = new System.Drawing.Font("Consolas", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_method.ForeColor = System.Drawing.Color.Black;
            this.txt_method.Location = new System.Drawing.Point(667, 295);
            this.txt_method.Name = "txt_method";
            this.txt_method.Size = new System.Drawing.Size(292, 32);
            this.txt_method.TabIndex = 23;
            // 
            // lbl_method
            // 
            this.lbl_method.AutoSize = true;
            this.lbl_method.Font = new System.Drawing.Font("Hobo Std", 21.75F, System.Drawing.FontStyle.Bold);
            this.lbl_method.ForeColor = System.Drawing.Color.MintCream;
            this.lbl_method.Location = new System.Drawing.Point(405, 286);
            this.lbl_method.Name = "lbl_method";
            this.lbl_method.Size = new System.Drawing.Size(140, 41);
            this.lbl_method.TabIndex = 22;
            this.lbl_method.Text = "Method :";
            // 
            // txt_class
            // 
            this.txt_class.Font = new System.Drawing.Font("Consolas", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_class.ForeColor = System.Drawing.Color.Black;
            this.txt_class.Location = new System.Drawing.Point(667, 245);
            this.txt_class.Name = "txt_class";
            this.txt_class.Size = new System.Drawing.Size(292, 32);
            this.txt_class.TabIndex = 21;
            // 
            // lbl_class
            // 
            this.lbl_class.AutoSize = true;
            this.lbl_class.Font = new System.Drawing.Font("Hobo Std", 21.75F, System.Drawing.FontStyle.Bold);
            this.lbl_class.ForeColor = System.Drawing.Color.MintCream;
            this.lbl_class.Location = new System.Drawing.Point(405, 236);
            this.lbl_class.Name = "lbl_class";
            this.lbl_class.Size = new System.Drawing.Size(111, 41);
            this.lbl_class.TabIndex = 20;
            this.lbl_class.Text = "Class :";
            // 
            // txt_project
            // 
            this.txt_project.Font = new System.Drawing.Font("Consolas", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_project.ForeColor = System.Drawing.Color.Black;
            this.txt_project.Location = new System.Drawing.Point(667, 193);
            this.txt_project.Name = "txt_project";
            this.txt_project.Size = new System.Drawing.Size(292, 32);
            this.txt_project.TabIndex = 19;
            // 
            // lbl_project
            // 
            this.lbl_project.AutoSize = true;
            this.lbl_project.Font = new System.Drawing.Font("Hobo Std", 21.75F, System.Drawing.FontStyle.Bold);
            this.lbl_project.ForeColor = System.Drawing.Color.MintCream;
            this.lbl_project.Location = new System.Drawing.Point(405, 184);
            this.lbl_project.Name = "lbl_project";
            this.lbl_project.Size = new System.Drawing.Size(137, 41);
            this.lbl_project.TabIndex = 18;
            this.lbl_project.Text = "Project :";
            // 
            // lbl_bugDescription
            // 
            this.lbl_bugDescription.AutoSize = true;
            this.lbl_bugDescription.Font = new System.Drawing.Font("Hobo Std", 21.75F, System.Drawing.FontStyle.Bold);
            this.lbl_bugDescription.ForeColor = System.Drawing.Color.MintCream;
            this.lbl_bugDescription.Location = new System.Drawing.Point(405, 39);
            this.lbl_bugDescription.Name = "lbl_bugDescription";
            this.lbl_bugDescription.Size = new System.Drawing.Size(256, 41);
            this.lbl_bugDescription.TabIndex = 16;
            this.lbl_bugDescription.Text = "Bug Description :";
            // 
            // lbl_bug
            // 
            this.lbl_bug.AutoSize = true;
            this.lbl_bug.Font = new System.Drawing.Font("Hobo Std", 21.75F, System.Drawing.FontStyle.Bold);
            this.lbl_bug.ForeColor = System.Drawing.Color.MintCream;
            this.lbl_bug.Location = new System.Drawing.Point(64, 39);
            this.lbl_bug.Name = "lbl_bug";
            this.lbl_bug.Size = new System.Drawing.Size(257, 41);
            this.lbl_bug.TabIndex = 15;
            this.lbl_bug.Text = "Bug Screenshot :";
            // 
            // pan_bug
            // 
            this.pan_bug.AutoScroll = true;
            this.pan_bug.Controls.Add(this.pictureBox_bug);
            this.pan_bug.Location = new System.Drawing.Point(12, 113);
            this.pan_bug.Name = "pan_bug";
            this.pan_bug.Size = new System.Drawing.Size(370, 363);
            this.pan_bug.TabIndex = 0;
            // 
            // pictureBox_bug
            // 
            this.pictureBox_bug.BackColor = System.Drawing.Color.White;
            this.pictureBox_bug.Location = new System.Drawing.Point(3, 3);
            this.pictureBox_bug.Name = "pictureBox_bug";
            this.pictureBox_bug.Size = new System.Drawing.Size(364, 357);
            this.pictureBox_bug.TabIndex = 1;
            this.pictureBox_bug.TabStop = false;
            // 
            // txt_bugDescription
            // 
            this.txt_bugDescription.Font = new System.Drawing.Font("Consolas", 15.75F);
            this.txt_bugDescription.Location = new System.Drawing.Point(667, 39);
            this.txt_bugDescription.Name = "txt_bugDescription";
            this.txt_bugDescription.Size = new System.Drawing.Size(292, 133);
            this.txt_bugDescription.TabIndex = 46;
            this.txt_bugDescription.Text = "";
            this.txt_bugDescription.TextChanged += new System.EventHandler(this.txt_bugDescription_TextChanged);
            // 
            // BugDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1338, 629);
            this.Controls.Add(this.pan_bugDetail);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "BugDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Bug Detail";
            this.pan_bugDetail.ResumeLayout(false);
            this.pan_bugDetail.PerformLayout();
            this.pan_bug.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_bug)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pan_bugDetail;
        private System.Windows.Forms.PictureBox pictureBox_bug;
        private System.Windows.Forms.Panel pan_bug;
        private System.Windows.Forms.Label lbl_bug;
        private System.Windows.Forms.Label lbl_bugDescription;
        private System.Windows.Forms.Label lbl_project;
        private System.Windows.Forms.TextBox txt_project;
        private System.Windows.Forms.Label lbl_class;
        private System.Windows.Forms.TextBox txt_class;
        private System.Windows.Forms.Label lbl_method;
        private System.Windows.Forms.TextBox txt_method;
        private System.Windows.Forms.Label lbl_lineNumber;
        private System.Windows.Forms.TextBox txt_lineNumber;
        private System.Windows.Forms.Label lbl_codeAuthor;
        private System.Windows.Forms.TextBox txt_codeAuthor;
        private System.Windows.Forms.TextBox txt_status;
        private System.Windows.Forms.Label lbl_status;
        private System.Windows.Forms.Label lbl_reportedDate;
        private System.Windows.Forms.TextBox txt_reportedDate;
        private System.Windows.Forms.TextBox txt_associateDeveloper;
        private System.Windows.Forms.Label lbl_associateDeveloper;
        private System.Windows.Forms.TextBox txt_markedAs;
        private System.Windows.Forms.Label lbl_markedAs;
        private System.Windows.Forms.TextBox txt_fixedDate;
        private System.Windows.Forms.Label lbl_fixedDate;
        private System.Windows.Forms.Button btn_update;
        private System.Windows.Forms.Button btn_cancel;
        private System.Windows.Forms.RichTextBox txt_bugDescription;
    }
}