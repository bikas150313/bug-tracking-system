﻿using BugTrackingSystem.edu.tbc77193461.controller;
using BugTrackingSystem.edu.tbc77193461.model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BugTrackingSystem.edu.tbc77193461.view
{
    public partial class DeveloperDashboard : Form
    {

        /// <summary>
        /// declaring required variables
        /// </summary>
        private string username;
        private int userId;

        public DeveloperDashboard()
        {
            InitializeComponent();
        }

        public DeveloperDashboard(string username, int userId)
        {
            this.username = username;
            this.userId = userId;
            this.WindowState = FormWindowState.Maximized;
            InitializeComponent();
        }

        /// <summary>
        /// event triggered on logout menu click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void menuItem_logout_Click(object sender, EventArgs e)
        {
            this.Hide();
            Login login = new Login();
            login.ShowDialog();
            this.Close();
        }

        /// <summary>
        /// event triggered on change password menu click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void menuItem_changePassword_Click(object sender, EventArgs e)
        {
            ChangePassword changePassword = new ChangePassword(username);
            changePassword.ShowDialog();
        }

        /// <summary>
        /// event triggered on view profile menu click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void menuItem_viewProfile_Click(object sender, EventArgs e)
        {
            User user = new User();
            user.UserId = userId;
            UserController userController = new UserController();
            userController.RetrieveUserData(user);
            string fullName = userController.fullName;
            string username = userController.username;
            string address = userController.address;
            string contactNo = userController.contactNo;
            string email = userController.email;
            string role = userController.role;
            UserProfile profile = new UserProfile(fullName, username, address, contactNo,
                email, role);
            profile.ShowDialog();
        }

        /// <summary>
        /// event triggered on report bug menu click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void menuItem_reportBug_Click(object sender, EventArgs e)
        {
            ReportBug reportBug = new ReportBug(userId);
            reportBug.ShowDialog();
        }

        /// <summary>
        /// event triggered on my bugs menu click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void menuItem_myBugs_Click(object sender, EventArgs e)
        {
            MyBug myBug = new MyBug(userId);
            myBug.ShowDialog();
        }

        /// <summary>
        /// event triggered on view active bugs menu click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void menuItem_viewActiveBug_Click(object sender, EventArgs e)
        {
            ActiveBug activeBug = new ActiveBug(userId);
            activeBug.ShowDialog();
        }

        /// <summary>
        /// open active bug detail form
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="bugId"></param>
        /// <param name="bugDescription"></param>
        /// <param name="project"></param>
        /// <param name="className"></param>
        /// <param name="method"></param>
        /// <param name="lineNo"></param>
        /// <param name="author"></param>
        /// <param name="memoryStream"></param>
        /// <param name="reportDate"></param>
        /// <param name="reporter"></param>
        public void OpenActiveBugDetailForm(int userId, int bugId, string bugDescription, string project, string className, 
            string method, string lineNo, string author, MemoryStream memoryStream, string reportDate, string reporter)
        {
            ActiveBugDetail activeBugDetail = new ActiveBugDetail(userId, bugId, bugDescription, project, className, method, 
                lineNo, author, memoryStream, reportDate, reporter);
            activeBugDetail.ShowDialog();
        }

        /// <summary>
        /// event triggered on to fix menu click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void menuItem_toFix_Click(object sender, EventArgs e)
        {
            ToFixList toFixList = new ToFixList(userId);
            toFixList.ShowDialog();
        }

        /// <summary>
        /// open to fix bug detail form
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="bugId"></param>
        /// <param name="bugDescription"></param>
        /// <param name="project"></param>
        /// <param name="className"></param>
        /// <param name="method"></param>
        /// <param name="lineNo"></param>
        /// <param name="author"></param>
        /// <param name="memoryStream"></param>
        /// <param name="reportDate"></param>
        /// <param name="reporter"></param>
        public void OpenToFixBugDetailForm(int userId, int bugId, string bugDescription, string project, string className,
            string method, string lineNo, string author, MemoryStream memoryStream, string reportDate, string reporter)
        {
            ToFixBugDetail toFixBugDetail = new ToFixBugDetail(userId, bugId, bugDescription, project, className, method,
                lineNo, author, memoryStream, reportDate, reporter);
            toFixBugDetail.ShowDialog();
        }

        /// <summary>
        /// event triggered on connect to bitbucket menu click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void menuItem_connectToBitbucket_Click(object sender, EventArgs e)
        {
            string url = "https://bitbucket.org/account/signin/";
            System.Diagnostics.Process.Start(url);
        }

        /// <summary>
        /// event triggered on about menu click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void menuItem_about_Click(object sender, EventArgs e)
        {
            About about = new About();
            about.ShowDialog();
        }
    }
}
