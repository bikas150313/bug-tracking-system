﻿using BugTrackingSystem.edu.tbc77193461.controller;
using BugTrackingSystem.edu.tbc77193461.model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BugTrackingSystem.edu.tbc77193461.view
{
    public partial class ActiveBug : Form
    {

        /// <summary>
        /// declaring required variables
        /// </summary>
        private int userId;
        BugController bugController;

        public ActiveBug(int userId)
        {
            this.userId = userId;
            InitializeComponent();
            //calling load active bugs method
            LoadActiveBugs();
        }

        /// <summary>
        /// load active bugs
        /// </summary>
        public void LoadActiveBugs()
        {
            bugController = new BugController();
            DataTable dataTable = bugController.LoadActiveBugs();
            //populating datagridview with data source
            dataGridView.DataSource = dataTable;
            //setting visibility to false for non required fields
            dataGridView.Columns["screenshot"].Visible = false;
            dataGridView.Columns["reporter_id"].Visible = false;
            dataGridView.Columns["fix_date"].Visible = false;
            dataGridView.Columns["fixer_id"].Visible = false;
            //editing columns header
            dataGridView.Columns["bug_id"].HeaderText = "Bug ID";
            dataGridView.Columns["bug_description"].HeaderText = "Bug Description";
            dataGridView.Columns["project"].HeaderText = "Project";
            dataGridView.Columns["class"].HeaderText = "Class";
            dataGridView.Columns["method"].HeaderText = "Method";
            dataGridView.Columns["line_number"].HeaderText = "Line Number";
            dataGridView.Columns["code_author"].HeaderText = "Code Author";
            dataGridView.Columns["report_date"].HeaderText = "Report Date";
            dataGridView.Columns["status"].HeaderText = "Status";
            //providing user edit right
            dataGridView.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView.AllowUserToAddRows = false;
            dataGridView.AllowUserToDeleteRows = false;
            dataGridView.MultiSelect = false;
            dataGridView.AllowUserToResizeRows = false;
            dataGridView.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing;
        }

        /// <summary>
        /// event triggered on update button click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_viewAndUpdateDetails_Click(object sender, EventArgs e)
        {
            MemoryStream memoryStream;
            if (dataGridView.SelectedRows.Count != 0)
            {
                //getting cell data
                int bugId = Convert.ToInt32(dataGridView.SelectedRows[0].Cells[0].Value.ToString());
                string bugDescription = dataGridView.SelectedRows[0].Cells[1].Value.ToString();
                string project = dataGridView.SelectedRows[0].Cells[2].Value.ToString();
                string className = CheckNull(dataGridView.SelectedRows[0].Cells[3].Value.ToString());
                string method = CheckNull(dataGridView.SelectedRows[0].Cells[4].Value.ToString());
                string lineNo = CheckNull(dataGridView.SelectedRows[0].Cells[5].Value.ToString());
                string author = CheckNull(dataGridView.SelectedRows[0].Cells[6].Value.ToString());
                byte[] screenshot = (byte[])dataGridView.SelectedRows[0].Cells[7].Value;
                string reportDate = dataGridView.SelectedRows[0].Cells[8].Value.ToString();
                int reporterId = Convert.ToInt32(dataGridView.SelectedRows[0].Cells[10].Value.ToString());
                memoryStream = new MemoryStream(screenshot);
                Bug bug = new Bug();
                bug.ReporterId = reporterId;
                string reporter = bugController.RetrieveReporter(bug);
                DeveloperDashboard dashboard = new DeveloperDashboard();
                dashboard.OpenActiveBugDetailForm(userId, bugId, bugDescription, project, className, method, lineNo,
                    author, memoryStream, reportDate, reporter);
            }
            else
            {
                MessageBox.Show("No row selected! Please select a row!");
            }
        }

        /// <summary>
        /// checking if cell data is null
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private string CheckNull(string value)
        {
            string defaultValue = "Not Specified";
            if (!string.IsNullOrEmpty(value))
            {
                defaultValue = value;
            }
            return defaultValue;
        }

        /// <summary>
        /// event triggered on refresh button click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_refresh_Click(object sender, EventArgs e)
        {
            //reload datagridview
            dataGridView.Refresh();
            LoadActiveBugs();
        }
    }
}
