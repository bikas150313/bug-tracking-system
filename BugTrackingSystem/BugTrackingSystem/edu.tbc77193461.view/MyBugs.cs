﻿using BugTrackingSystem.edu.tbc77193461.controller;
using BugTrackingSystem.edu.tbc77193461.model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BugTrackingSystem.edu.tbc77193461.view
{
    public partial class MyBug : Form
    {

        /// <summary>
        /// declaring required variables
        /// </summary>
        private int userId;
        private BugController bugController;

        public MyBug(int userId)
        {
            this.userId = userId;
            InitializeComponent();
            LoadMyBugs();
        }

        /// <summary>
        /// load personal bugs
        /// </summary>
        public void LoadMyBugs()
        {
            bugController = new BugController();
            DataTable dataTable = bugController.LoadMyBugs(userId);
            //populating datagridview with source
            dataGridView.DataSource = dataTable;
            //setting columns visibility
            dataGridView.Columns["screenshot"].Visible = false;
            dataGridView.Columns["reporter_id"].Visible = false;
            dataGridView.Columns["fix_date"].Visible = false;
            dataGridView.Columns["fixer_id"].Visible = false;
            //editing columns header text
            dataGridView.Columns["bug_id"].HeaderText = "Bug ID";
            dataGridView.Columns["bug_description"].HeaderText = "Bug Description";
            dataGridView.Columns["project"].HeaderText = "Project";
            dataGridView.Columns["class"].HeaderText = "Class";
            dataGridView.Columns["method"].HeaderText = "Method";
            dataGridView.Columns["line_number"].HeaderText = "Line Number";
            dataGridView.Columns["code_author"].HeaderText = "Code Author";
            dataGridView.Columns["report_date"].HeaderText = "Report Date";
            dataGridView.Columns["status"].HeaderText = "Status";
            //setting user edit role
            dataGridView.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView.AllowUserToAddRows = false;
            dataGridView.AllowUserToDeleteRows = false;
            dataGridView.MultiSelect = false;
            dataGridView.AllowUserToResizeRows = false;
            dataGridView.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing;
        }

        /// <summary>
        /// event triggered on view details button click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_viewDetails_Click(object sender, EventArgs e)
        {
            MemoryStream memoryStream;
            if (dataGridView.SelectedRows.Count != 0)
            {
                //gathering cell value
                int bugId = Convert.ToInt32(dataGridView.SelectedRows[0].Cells[0].Value.ToString());
                string bugDescription = dataGridView.SelectedRows[0].Cells[1].Value.ToString();
                string project = dataGridView.SelectedRows[0].Cells[2].Value.ToString();
                string className = CheckNull(dataGridView.SelectedRows[0].Cells[3].Value.ToString());
                string method = CheckNull(dataGridView.SelectedRows[0].Cells[4].Value.ToString());
                string lineNo = CheckNull(dataGridView.SelectedRows[0].Cells[5].Value.ToString());
                string author = CheckNull(dataGridView.SelectedRows[0].Cells[6].Value.ToString());
                byte[] screenshot = (byte[]) dataGridView.SelectedRows[0].Cells[7].Value;
                string reportDate = dataGridView.SelectedRows[0].Cells[8].Value.ToString();
                string status = dataGridView.SelectedRows[0].Cells[9].Value.ToString();
                string fixDate = CheckNull(dataGridView.SelectedRows[0].Cells[11].Value.ToString());
                string fixerId = CheckNull(dataGridView.SelectedRows[0].Cells[12].Value.ToString());
                memoryStream = new MemoryStream(screenshot);
                //checking fixer id and bug status
                if (fixerId!="Not Specified" && status!="Active")
                {
                    int fixerIntId = Convert.ToInt32(fixerId);
                    Bug bug = new Bug();
                    bug.FixerId = fixerIntId;
                    string fixer = bugController.RetrieveFixer(bug);
                    TesterDashboard dashboard = new TesterDashboard();
                    dashboard.OpenBugDetailFormWithFixer(bugId, bugDescription, project, className, method, lineNo,
                        author, memoryStream, reportDate, status, fixDate, fixer);
                }
                else
                {
                    string markedAs = "Not Specified";
                    TesterDashboard dashboard = new TesterDashboard();
                    dashboard.OpenBugDetailFormWithoutFixer(bugId, bugDescription, project, className, method, lineNo,
                        author, memoryStream, reportDate, status, fixDate, fixerId, markedAs);
                }
            }
            else
            {
                MessageBox.Show("No row selected! Please select a row!");
            }
        }

        /// <summary>
        /// checking null value
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private string CheckNull(string value)
        {
            string defaultValue = "Not Specified";
            if (!string.IsNullOrEmpty(value))
            {
                defaultValue = value;
            }
            return defaultValue;
        }

        /// <summary>
        /// event triggered on refresh button click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_refresh_Click(object sender, EventArgs e)
        {
            //refresh datagridview
            dataGridView.Refresh();
            LoadMyBugs();
        }
    }
}
