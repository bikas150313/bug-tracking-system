﻿namespace BugTrackingSystem.edu.tbc77193461.view
{
    partial class ActiveBugDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ActiveBugDetail));
            this.pan_bugDetail = new System.Windows.Forms.Panel();
            this.combo_year = new System.Windows.Forms.ComboBox();
            this.combo_date = new System.Windows.Forms.ComboBox();
            this.combo_month = new System.Windows.Forms.ComboBox();
            this.lbl_fixedDate = new System.Windows.Forms.Label();
            this.combo_markAs = new System.Windows.Forms.ComboBox();
            this.btn_cancel = new System.Windows.Forms.Button();
            this.btn_update = new System.Windows.Forms.Button();
            this.lbl_markAs = new System.Windows.Forms.Label();
            this.txt_reportedBy = new System.Windows.Forms.TextBox();
            this.lbl_reportedBy = new System.Windows.Forms.Label();
            this.txt_reportedDate = new System.Windows.Forms.TextBox();
            this.lbl_reportedDate = new System.Windows.Forms.Label();
            this.txt_codeAuthor = new System.Windows.Forms.TextBox();
            this.lbl_codeAuthor = new System.Windows.Forms.Label();
            this.txt_lineNumber = new System.Windows.Forms.TextBox();
            this.lbl_lineNumber = new System.Windows.Forms.Label();
            this.txt_method = new System.Windows.Forms.TextBox();
            this.lbl_method = new System.Windows.Forms.Label();
            this.txt_class = new System.Windows.Forms.TextBox();
            this.lbl_class = new System.Windows.Forms.Label();
            this.txt_project = new System.Windows.Forms.TextBox();
            this.lbl_project = new System.Windows.Forms.Label();
            this.lbl_bugDescription = new System.Windows.Forms.Label();
            this.lbl_bug = new System.Windows.Forms.Label();
            this.pan_bug = new System.Windows.Forms.Panel();
            this.pictureBox_bug = new System.Windows.Forms.PictureBox();
            this.txt_bugDescription = new System.Windows.Forms.RichTextBox();
            this.pan_bugDetail.SuspendLayout();
            this.pan_bug.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_bug)).BeginInit();
            this.SuspendLayout();
            // 
            // pan_bugDetail
            // 
            this.pan_bugDetail.AutoScroll = true;
            this.pan_bugDetail.BackColor = System.Drawing.Color.Black;
            this.pan_bugDetail.Controls.Add(this.txt_bugDescription);
            this.pan_bugDetail.Controls.Add(this.combo_year);
            this.pan_bugDetail.Controls.Add(this.combo_date);
            this.pan_bugDetail.Controls.Add(this.combo_month);
            this.pan_bugDetail.Controls.Add(this.lbl_fixedDate);
            this.pan_bugDetail.Controls.Add(this.combo_markAs);
            this.pan_bugDetail.Controls.Add(this.btn_cancel);
            this.pan_bugDetail.Controls.Add(this.btn_update);
            this.pan_bugDetail.Controls.Add(this.lbl_markAs);
            this.pan_bugDetail.Controls.Add(this.txt_reportedBy);
            this.pan_bugDetail.Controls.Add(this.lbl_reportedBy);
            this.pan_bugDetail.Controls.Add(this.txt_reportedDate);
            this.pan_bugDetail.Controls.Add(this.lbl_reportedDate);
            this.pan_bugDetail.Controls.Add(this.txt_codeAuthor);
            this.pan_bugDetail.Controls.Add(this.lbl_codeAuthor);
            this.pan_bugDetail.Controls.Add(this.txt_lineNumber);
            this.pan_bugDetail.Controls.Add(this.lbl_lineNumber);
            this.pan_bugDetail.Controls.Add(this.txt_method);
            this.pan_bugDetail.Controls.Add(this.lbl_method);
            this.pan_bugDetail.Controls.Add(this.txt_class);
            this.pan_bugDetail.Controls.Add(this.lbl_class);
            this.pan_bugDetail.Controls.Add(this.txt_project);
            this.pan_bugDetail.Controls.Add(this.lbl_project);
            this.pan_bugDetail.Controls.Add(this.lbl_bugDescription);
            this.pan_bugDetail.Controls.Add(this.lbl_bug);
            this.pan_bugDetail.Controls.Add(this.pan_bug);
            this.pan_bugDetail.Location = new System.Drawing.Point(0, 0);
            this.pan_bugDetail.Name = "pan_bugDetail";
            this.pan_bugDetail.Size = new System.Drawing.Size(1339, 629);
            this.pan_bugDetail.TabIndex = 1;
            // 
            // combo_year
            // 
            this.combo_year.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combo_year.Font = new System.Drawing.Font("Consolas", 15.75F);
            this.combo_year.FormattingEnabled = true;
            this.combo_year.Items.AddRange(new object[] {
            "2018",
            "2019",
            "2020",
            "2021",
            "2022",
            "2023",
            "2024",
            "2025",
            "2026",
            "2027",
            "2028",
            "2029",
            "2030"});
            this.combo_year.Location = new System.Drawing.Point(871, 507);
            this.combo_year.Name = "combo_year";
            this.combo_year.Size = new System.Drawing.Size(88, 32);
            this.combo_year.TabIndex = 44;
            // 
            // combo_date
            // 
            this.combo_date.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combo_date.Font = new System.Drawing.Font("Consolas", 15.75F);
            this.combo_date.FormattingEnabled = true;
            this.combo_date.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24",
            "25",
            "26",
            "27",
            "28",
            "29",
            "30",
            "31",
            "32"});
            this.combo_date.Location = new System.Drawing.Point(768, 507);
            this.combo_date.Name = "combo_date";
            this.combo_date.Size = new System.Drawing.Size(86, 32);
            this.combo_date.TabIndex = 43;
            // 
            // combo_month
            // 
            this.combo_month.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combo_month.Font = new System.Drawing.Font("Consolas", 15.75F);
            this.combo_month.FormattingEnabled = true;
            this.combo_month.Items.AddRange(new object[] {
            "JAN",
            "FEB",
            "MAR",
            "APR",
            "MAY",
            "JUN",
            "JUL",
            "AUG",
            "SEP",
            "OCT",
            "NOV",
            "DEC"});
            this.combo_month.Location = new System.Drawing.Point(667, 507);
            this.combo_month.Name = "combo_month";
            this.combo_month.Size = new System.Drawing.Size(86, 32);
            this.combo_month.TabIndex = 42;
            // 
            // lbl_fixedDate
            // 
            this.lbl_fixedDate.AutoSize = true;
            this.lbl_fixedDate.Font = new System.Drawing.Font("Hobo Std", 21.75F, System.Drawing.FontStyle.Bold);
            this.lbl_fixedDate.ForeColor = System.Drawing.Color.MintCream;
            this.lbl_fixedDate.Location = new System.Drawing.Point(405, 498);
            this.lbl_fixedDate.Name = "lbl_fixedDate";
            this.lbl_fixedDate.Size = new System.Drawing.Size(182, 41);
            this.lbl_fixedDate.TabIndex = 41;
            this.lbl_fixedDate.Text = "Fixed Date :";
            // 
            // combo_markAs
            // 
            this.combo_markAs.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combo_markAs.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.combo_markAs.FormattingEnabled = true;
            this.combo_markAs.Items.AddRange(new object[] {
            "Fixing",
            "Fixed"});
            this.combo_markAs.Location = new System.Drawing.Point(667, 452);
            this.combo_markAs.Name = "combo_markAs";
            this.combo_markAs.Size = new System.Drawing.Size(292, 33);
            this.combo_markAs.TabIndex = 40;
            // 
            // btn_cancel
            // 
            this.btn_cancel.Font = new System.Drawing.Font("Algerian", 15.75F);
            this.btn_cancel.ForeColor = System.Drawing.Color.Red;
            this.btn_cancel.Image = ((System.Drawing.Image)(resources.GetObject("btn_cancel.Image")));
            this.btn_cancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_cancel.Location = new System.Drawing.Point(736, 581);
            this.btn_cancel.Name = "btn_cancel";
            this.btn_cancel.Size = new System.Drawing.Size(118, 36);
            this.btn_cancel.TabIndex = 39;
            this.btn_cancel.Text = "Cancel";
            this.btn_cancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_cancel.UseVisualStyleBackColor = true;
            this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
            // 
            // btn_update
            // 
            this.btn_update.Font = new System.Drawing.Font("Algerian", 15.75F);
            this.btn_update.ForeColor = System.Drawing.Color.Green;
            this.btn_update.Image = ((System.Drawing.Image)(resources.GetObject("btn_update.Image")));
            this.btn_update.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_update.Location = new System.Drawing.Point(461, 581);
            this.btn_update.Name = "btn_update";
            this.btn_update.Size = new System.Drawing.Size(122, 36);
            this.btn_update.TabIndex = 38;
            this.btn_update.Text = "Update";
            this.btn_update.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_update.UseVisualStyleBackColor = true;
            this.btn_update.Click += new System.EventHandler(this.btn_update_Click);
            // 
            // lbl_markAs
            // 
            this.lbl_markAs.AutoSize = true;
            this.lbl_markAs.Font = new System.Drawing.Font("Hobo Std", 21.75F, System.Drawing.FontStyle.Bold);
            this.lbl_markAs.ForeColor = System.Drawing.Color.MintCream;
            this.lbl_markAs.Location = new System.Drawing.Point(405, 444);
            this.lbl_markAs.Name = "lbl_markAs";
            this.lbl_markAs.Size = new System.Drawing.Size(153, 41);
            this.lbl_markAs.TabIndex = 34;
            this.lbl_markAs.Text = "Mark As :";
            // 
            // txt_reportedBy
            // 
            this.txt_reportedBy.Font = new System.Drawing.Font("Consolas", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_reportedBy.ForeColor = System.Drawing.Color.Blue;
            this.txt_reportedBy.Location = new System.Drawing.Point(995, 113);
            this.txt_reportedBy.Name = "txt_reportedBy";
            this.txt_reportedBy.ReadOnly = true;
            this.txt_reportedBy.Size = new System.Drawing.Size(312, 32);
            this.txt_reportedBy.TabIndex = 33;
            // 
            // lbl_reportedBy
            // 
            this.lbl_reportedBy.AutoSize = true;
            this.lbl_reportedBy.Font = new System.Drawing.Font("Hobo Std", 21.75F, System.Drawing.FontStyle.Bold);
            this.lbl_reportedBy.ForeColor = System.Drawing.Color.MintCream;
            this.lbl_reportedBy.Location = new System.Drawing.Point(988, 39);
            this.lbl_reportedBy.Name = "lbl_reportedBy";
            this.lbl_reportedBy.Size = new System.Drawing.Size(208, 41);
            this.lbl_reportedBy.TabIndex = 32;
            this.lbl_reportedBy.Text = "Reported By :";
            // 
            // txt_reportedDate
            // 
            this.txt_reportedDate.Font = new System.Drawing.Font("Consolas", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_reportedDate.ForeColor = System.Drawing.Color.Blue;
            this.txt_reportedDate.Location = new System.Drawing.Point(995, 245);
            this.txt_reportedDate.Name = "txt_reportedDate";
            this.txt_reportedDate.ReadOnly = true;
            this.txt_reportedDate.Size = new System.Drawing.Size(292, 32);
            this.txt_reportedDate.TabIndex = 31;
            // 
            // lbl_reportedDate
            // 
            this.lbl_reportedDate.AutoSize = true;
            this.lbl_reportedDate.Font = new System.Drawing.Font("Hobo Std", 21.75F, System.Drawing.FontStyle.Bold);
            this.lbl_reportedDate.ForeColor = System.Drawing.Color.MintCream;
            this.lbl_reportedDate.Location = new System.Drawing.Point(988, 184);
            this.lbl_reportedDate.Name = "lbl_reportedDate";
            this.lbl_reportedDate.Size = new System.Drawing.Size(235, 41);
            this.lbl_reportedDate.TabIndex = 30;
            this.lbl_reportedDate.Text = "Reported Date :";
            // 
            // txt_codeAuthor
            // 
            this.txt_codeAuthor.Font = new System.Drawing.Font("Consolas", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_codeAuthor.ForeColor = System.Drawing.Color.Blue;
            this.txt_codeAuthor.Location = new System.Drawing.Point(667, 398);
            this.txt_codeAuthor.Name = "txt_codeAuthor";
            this.txt_codeAuthor.ReadOnly = true;
            this.txt_codeAuthor.Size = new System.Drawing.Size(292, 32);
            this.txt_codeAuthor.TabIndex = 27;
            // 
            // lbl_codeAuthor
            // 
            this.lbl_codeAuthor.AutoSize = true;
            this.lbl_codeAuthor.Font = new System.Drawing.Font("Hobo Std", 21.75F, System.Drawing.FontStyle.Bold);
            this.lbl_codeAuthor.ForeColor = System.Drawing.Color.MintCream;
            this.lbl_codeAuthor.Location = new System.Drawing.Point(405, 389);
            this.lbl_codeAuthor.Name = "lbl_codeAuthor";
            this.lbl_codeAuthor.Size = new System.Drawing.Size(210, 41);
            this.lbl_codeAuthor.TabIndex = 26;
            this.lbl_codeAuthor.Text = "Code Author :";
            // 
            // txt_lineNumber
            // 
            this.txt_lineNumber.Font = new System.Drawing.Font("Consolas", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_lineNumber.ForeColor = System.Drawing.Color.Blue;
            this.txt_lineNumber.Location = new System.Drawing.Point(667, 347);
            this.txt_lineNumber.Name = "txt_lineNumber";
            this.txt_lineNumber.ReadOnly = true;
            this.txt_lineNumber.Size = new System.Drawing.Size(292, 32);
            this.txt_lineNumber.TabIndex = 25;
            // 
            // lbl_lineNumber
            // 
            this.lbl_lineNumber.AutoSize = true;
            this.lbl_lineNumber.Font = new System.Drawing.Font("Hobo Std", 21.75F, System.Drawing.FontStyle.Bold);
            this.lbl_lineNumber.ForeColor = System.Drawing.Color.MintCream;
            this.lbl_lineNumber.Location = new System.Drawing.Point(405, 338);
            this.lbl_lineNumber.Name = "lbl_lineNumber";
            this.lbl_lineNumber.Size = new System.Drawing.Size(211, 41);
            this.lbl_lineNumber.TabIndex = 24;
            this.lbl_lineNumber.Text = "Line Number :";
            // 
            // txt_method
            // 
            this.txt_method.Font = new System.Drawing.Font("Consolas", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_method.ForeColor = System.Drawing.Color.Blue;
            this.txt_method.Location = new System.Drawing.Point(667, 295);
            this.txt_method.Name = "txt_method";
            this.txt_method.ReadOnly = true;
            this.txt_method.Size = new System.Drawing.Size(292, 32);
            this.txt_method.TabIndex = 23;
            // 
            // lbl_method
            // 
            this.lbl_method.AutoSize = true;
            this.lbl_method.Font = new System.Drawing.Font("Hobo Std", 21.75F, System.Drawing.FontStyle.Bold);
            this.lbl_method.ForeColor = System.Drawing.Color.MintCream;
            this.lbl_method.Location = new System.Drawing.Point(405, 286);
            this.lbl_method.Name = "lbl_method";
            this.lbl_method.Size = new System.Drawing.Size(140, 41);
            this.lbl_method.TabIndex = 22;
            this.lbl_method.Text = "Method :";
            // 
            // txt_class
            // 
            this.txt_class.Font = new System.Drawing.Font("Consolas", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_class.ForeColor = System.Drawing.Color.Blue;
            this.txt_class.Location = new System.Drawing.Point(667, 245);
            this.txt_class.Name = "txt_class";
            this.txt_class.ReadOnly = true;
            this.txt_class.Size = new System.Drawing.Size(292, 32);
            this.txt_class.TabIndex = 21;
            // 
            // lbl_class
            // 
            this.lbl_class.AutoSize = true;
            this.lbl_class.Font = new System.Drawing.Font("Hobo Std", 21.75F, System.Drawing.FontStyle.Bold);
            this.lbl_class.ForeColor = System.Drawing.Color.MintCream;
            this.lbl_class.Location = new System.Drawing.Point(405, 236);
            this.lbl_class.Name = "lbl_class";
            this.lbl_class.Size = new System.Drawing.Size(111, 41);
            this.lbl_class.TabIndex = 20;
            this.lbl_class.Text = "Class :";
            // 
            // txt_project
            // 
            this.txt_project.Font = new System.Drawing.Font("Consolas", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_project.ForeColor = System.Drawing.Color.Blue;
            this.txt_project.Location = new System.Drawing.Point(667, 193);
            this.txt_project.Name = "txt_project";
            this.txt_project.ReadOnly = true;
            this.txt_project.Size = new System.Drawing.Size(292, 32);
            this.txt_project.TabIndex = 19;
            // 
            // lbl_project
            // 
            this.lbl_project.AutoSize = true;
            this.lbl_project.Font = new System.Drawing.Font("Hobo Std", 21.75F, System.Drawing.FontStyle.Bold);
            this.lbl_project.ForeColor = System.Drawing.Color.MintCream;
            this.lbl_project.Location = new System.Drawing.Point(405, 184);
            this.lbl_project.Name = "lbl_project";
            this.lbl_project.Size = new System.Drawing.Size(137, 41);
            this.lbl_project.TabIndex = 18;
            this.lbl_project.Text = "Project :";
            // 
            // lbl_bugDescription
            // 
            this.lbl_bugDescription.AutoSize = true;
            this.lbl_bugDescription.Font = new System.Drawing.Font("Hobo Std", 21.75F, System.Drawing.FontStyle.Bold);
            this.lbl_bugDescription.ForeColor = System.Drawing.Color.MintCream;
            this.lbl_bugDescription.Location = new System.Drawing.Point(405, 39);
            this.lbl_bugDescription.Name = "lbl_bugDescription";
            this.lbl_bugDescription.Size = new System.Drawing.Size(256, 41);
            this.lbl_bugDescription.TabIndex = 16;
            this.lbl_bugDescription.Text = "Bug Description :";
            // 
            // lbl_bug
            // 
            this.lbl_bug.AutoSize = true;
            this.lbl_bug.Font = new System.Drawing.Font("Hobo Std", 21.75F, System.Drawing.FontStyle.Bold);
            this.lbl_bug.ForeColor = System.Drawing.Color.MintCream;
            this.lbl_bug.Location = new System.Drawing.Point(64, 39);
            this.lbl_bug.Name = "lbl_bug";
            this.lbl_bug.Size = new System.Drawing.Size(257, 41);
            this.lbl_bug.TabIndex = 15;
            this.lbl_bug.Text = "Bug Screenshot :";
            // 
            // pan_bug
            // 
            this.pan_bug.AutoScroll = true;
            this.pan_bug.Controls.Add(this.pictureBox_bug);
            this.pan_bug.Location = new System.Drawing.Point(12, 113);
            this.pan_bug.Name = "pan_bug";
            this.pan_bug.Size = new System.Drawing.Size(370, 363);
            this.pan_bug.TabIndex = 0;
            // 
            // pictureBox_bug
            // 
            this.pictureBox_bug.BackColor = System.Drawing.Color.White;
            this.pictureBox_bug.Location = new System.Drawing.Point(3, 3);
            this.pictureBox_bug.Name = "pictureBox_bug";
            this.pictureBox_bug.Size = new System.Drawing.Size(364, 357);
            this.pictureBox_bug.TabIndex = 1;
            this.pictureBox_bug.TabStop = false;
            // 
            // txt_bugDescription
            // 
            this.txt_bugDescription.Font = new System.Drawing.Font("Consolas", 15.75F);
            this.txt_bugDescription.Location = new System.Drawing.Point(667, 39);
            this.txt_bugDescription.Name = "txt_bugDescription";
            this.txt_bugDescription.Size = new System.Drawing.Size(292, 133);
            this.txt_bugDescription.TabIndex = 45;
            this.txt_bugDescription.Text = "";
            this.txt_bugDescription.TextChanged += new System.EventHandler(this.txt_bugDescription_TextChanged);
            // 
            // ActiveBugDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1338, 629);
            this.Controls.Add(this.pan_bugDetail);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ActiveBugDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Active Bug Detail";
            this.pan_bugDetail.ResumeLayout(false);
            this.pan_bugDetail.PerformLayout();
            this.pan_bug.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_bug)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pan_bugDetail;
        private System.Windows.Forms.Button btn_cancel;
        private System.Windows.Forms.Button btn_update;
        private System.Windows.Forms.Label lbl_markAs;
        private System.Windows.Forms.TextBox txt_reportedBy;
        private System.Windows.Forms.Label lbl_reportedBy;
        private System.Windows.Forms.TextBox txt_reportedDate;
        private System.Windows.Forms.Label lbl_reportedDate;
        private System.Windows.Forms.TextBox txt_codeAuthor;
        private System.Windows.Forms.Label lbl_codeAuthor;
        private System.Windows.Forms.TextBox txt_lineNumber;
        private System.Windows.Forms.Label lbl_lineNumber;
        private System.Windows.Forms.TextBox txt_method;
        private System.Windows.Forms.Label lbl_method;
        private System.Windows.Forms.TextBox txt_class;
        private System.Windows.Forms.Label lbl_class;
        private System.Windows.Forms.TextBox txt_project;
        private System.Windows.Forms.Label lbl_project;
        private System.Windows.Forms.Label lbl_bugDescription;
        private System.Windows.Forms.Label lbl_bug;
        private System.Windows.Forms.Panel pan_bug;
        private System.Windows.Forms.PictureBox pictureBox_bug;
        private System.Windows.Forms.ComboBox combo_markAs;
        private System.Windows.Forms.Label lbl_fixedDate;
        private System.Windows.Forms.ComboBox combo_month;
        private System.Windows.Forms.ComboBox combo_date;
        private System.Windows.Forms.ComboBox combo_year;
        private System.Windows.Forms.RichTextBox txt_bugDescription;
    }
}