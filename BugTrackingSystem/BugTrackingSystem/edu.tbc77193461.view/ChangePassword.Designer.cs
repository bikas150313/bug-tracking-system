﻿namespace BugTrackingSystem
{
    partial class ChangePassword
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ChangePassword));
            this.pan_changePassword = new System.Windows.Forms.Panel();
            this.btn_cancel = new System.Windows.Forms.Button();
            this.btn_update = new System.Windows.Forms.Button();
            this.txt_confirmPassword = new System.Windows.Forms.TextBox();
            this.lbl_confirmPassword = new System.Windows.Forms.Label();
            this.txt_newPassword = new System.Windows.Forms.TextBox();
            this.lbl_newPassword = new System.Windows.Forms.Label();
            this.txt_currentPassword = new System.Windows.Forms.TextBox();
            this.lbl_currentPassword = new System.Windows.Forms.Label();
            this.txt_userName = new System.Windows.Forms.TextBox();
            this.lbl_userName = new System.Windows.Forms.Label();
            this.lbl_changePassword = new System.Windows.Forms.Label();
            this.pan_changePassword.SuspendLayout();
            this.SuspendLayout();
            // 
            // pan_changePassword
            // 
            this.pan_changePassword.BackColor = System.Drawing.Color.Black;
            this.pan_changePassword.Controls.Add(this.btn_cancel);
            this.pan_changePassword.Controls.Add(this.btn_update);
            this.pan_changePassword.Controls.Add(this.txt_confirmPassword);
            this.pan_changePassword.Controls.Add(this.lbl_confirmPassword);
            this.pan_changePassword.Controls.Add(this.txt_newPassword);
            this.pan_changePassword.Controls.Add(this.lbl_newPassword);
            this.pan_changePassword.Controls.Add(this.txt_currentPassword);
            this.pan_changePassword.Controls.Add(this.lbl_currentPassword);
            this.pan_changePassword.Controls.Add(this.txt_userName);
            this.pan_changePassword.Controls.Add(this.lbl_userName);
            this.pan_changePassword.Controls.Add(this.lbl_changePassword);
            this.pan_changePassword.Location = new System.Drawing.Point(0, 0);
            this.pan_changePassword.Name = "pan_changePassword";
            this.pan_changePassword.Size = new System.Drawing.Size(566, 370);
            this.pan_changePassword.TabIndex = 0;
            // 
            // btn_cancel
            // 
            this.btn_cancel.Font = new System.Drawing.Font("Algerian", 15.75F);
            this.btn_cancel.ForeColor = System.Drawing.Color.Red;
            this.btn_cancel.Image = ((System.Drawing.Image)(resources.GetObject("btn_cancel.Image")));
            this.btn_cancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_cancel.Location = new System.Drawing.Point(362, 306);
            this.btn_cancel.Name = "btn_cancel";
            this.btn_cancel.Size = new System.Drawing.Size(118, 36);
            this.btn_cancel.TabIndex = 10;
            this.btn_cancel.Text = "Cancel";
            this.btn_cancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_cancel.UseVisualStyleBackColor = true;
            this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
            // 
            // btn_update
            // 
            this.btn_update.Font = new System.Drawing.Font("Algerian", 15.75F);
            this.btn_update.ForeColor = System.Drawing.Color.Green;
            this.btn_update.Image = ((System.Drawing.Image)(resources.GetObject("btn_update.Image")));
            this.btn_update.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_update.Location = new System.Drawing.Point(82, 306);
            this.btn_update.Name = "btn_update";
            this.btn_update.Size = new System.Drawing.Size(120, 36);
            this.btn_update.TabIndex = 9;
            this.btn_update.Text = "Update";
            this.btn_update.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_update.UseVisualStyleBackColor = true;
            this.btn_update.Click += new System.EventHandler(this.btn_update_Click);
            // 
            // txt_confirmPassword
            // 
            this.txt_confirmPassword.Font = new System.Drawing.Font("Consolas", 15.75F);
            this.txt_confirmPassword.ForeColor = System.Drawing.Color.Blue;
            this.txt_confirmPassword.Location = new System.Drawing.Point(313, 227);
            this.txt_confirmPassword.Name = "txt_confirmPassword";
            this.txt_confirmPassword.Size = new System.Drawing.Size(234, 32);
            this.txt_confirmPassword.TabIndex = 8;
            this.txt_confirmPassword.UseSystemPasswordChar = true;
            // 
            // lbl_confirmPassword
            // 
            this.lbl_confirmPassword.AutoSize = true;
            this.lbl_confirmPassword.Font = new System.Drawing.Font("Hobo Std", 21.75F, System.Drawing.FontStyle.Bold);
            this.lbl_confirmPassword.ForeColor = System.Drawing.Color.MintCream;
            this.lbl_confirmPassword.Location = new System.Drawing.Point(12, 218);
            this.lbl_confirmPassword.Name = "lbl_confirmPassword";
            this.lbl_confirmPassword.Size = new System.Drawing.Size(291, 41);
            this.lbl_confirmPassword.TabIndex = 7;
            this.lbl_confirmPassword.Text = "Confirm Password :";
            // 
            // txt_newPassword
            // 
            this.txt_newPassword.Font = new System.Drawing.Font("Consolas", 15.75F);
            this.txt_newPassword.ForeColor = System.Drawing.Color.Blue;
            this.txt_newPassword.Location = new System.Drawing.Point(313, 171);
            this.txt_newPassword.Name = "txt_newPassword";
            this.txt_newPassword.Size = new System.Drawing.Size(234, 32);
            this.txt_newPassword.TabIndex = 6;
            this.txt_newPassword.UseSystemPasswordChar = true;
            // 
            // lbl_newPassword
            // 
            this.lbl_newPassword.AutoSize = true;
            this.lbl_newPassword.Font = new System.Drawing.Font("Hobo Std", 21.75F, System.Drawing.FontStyle.Bold);
            this.lbl_newPassword.ForeColor = System.Drawing.Color.MintCream;
            this.lbl_newPassword.Location = new System.Drawing.Point(12, 161);
            this.lbl_newPassword.Name = "lbl_newPassword";
            this.lbl_newPassword.Size = new System.Drawing.Size(240, 41);
            this.lbl_newPassword.TabIndex = 5;
            this.lbl_newPassword.Text = "New Password :";
            // 
            // txt_currentPassword
            // 
            this.txt_currentPassword.Font = new System.Drawing.Font("Consolas", 15.75F);
            this.txt_currentPassword.ForeColor = System.Drawing.Color.Blue;
            this.txt_currentPassword.Location = new System.Drawing.Point(313, 118);
            this.txt_currentPassword.Name = "txt_currentPassword";
            this.txt_currentPassword.Size = new System.Drawing.Size(234, 32);
            this.txt_currentPassword.TabIndex = 4;
            this.txt_currentPassword.UseSystemPasswordChar = true;
            // 
            // lbl_currentPassword
            // 
            this.lbl_currentPassword.AutoSize = true;
            this.lbl_currentPassword.Font = new System.Drawing.Font("Hobo Std", 21.75F, System.Drawing.FontStyle.Bold);
            this.lbl_currentPassword.ForeColor = System.Drawing.Color.MintCream;
            this.lbl_currentPassword.Location = new System.Drawing.Point(12, 109);
            this.lbl_currentPassword.Name = "lbl_currentPassword";
            this.lbl_currentPassword.Size = new System.Drawing.Size(288, 41);
            this.lbl_currentPassword.TabIndex = 3;
            this.lbl_currentPassword.Text = "Current Password :";
            // 
            // txt_userName
            // 
            this.txt_userName.Cursor = System.Windows.Forms.Cursors.No;
            this.txt_userName.Enabled = false;
            this.txt_userName.Font = new System.Drawing.Font("Consolas", 15.75F);
            this.txt_userName.ForeColor = System.Drawing.Color.Blue;
            this.txt_userName.Location = new System.Drawing.Point(313, 67);
            this.txt_userName.Name = "txt_userName";
            this.txt_userName.ReadOnly = true;
            this.txt_userName.Size = new System.Drawing.Size(234, 32);
            this.txt_userName.TabIndex = 2;
            // 
            // lbl_userName
            // 
            this.lbl_userName.AutoSize = true;
            this.lbl_userName.Font = new System.Drawing.Font("Hobo Std", 21.75F, System.Drawing.FontStyle.Bold);
            this.lbl_userName.ForeColor = System.Drawing.Color.MintCream;
            this.lbl_userName.Location = new System.Drawing.Point(12, 58);
            this.lbl_userName.Name = "lbl_userName";
            this.lbl_userName.Size = new System.Drawing.Size(176, 41);
            this.lbl_userName.TabIndex = 1;
            this.lbl_userName.Text = "Username :";
            // 
            // lbl_changePassword
            // 
            this.lbl_changePassword.AutoSize = true;
            this.lbl_changePassword.Font = new System.Drawing.Font("Cooper Black", 21.75F);
            this.lbl_changePassword.ForeColor = System.Drawing.Color.Aqua;
            this.lbl_changePassword.Location = new System.Drawing.Point(159, 9);
            this.lbl_changePassword.Name = "lbl_changePassword";
            this.lbl_changePassword.Size = new System.Drawing.Size(275, 34);
            this.lbl_changePassword.TabIndex = 0;
            this.lbl_changePassword.Text = "Change Password";
            // 
            // ChangePassword
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(566, 370);
            this.Controls.Add(this.pan_changePassword);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ChangePassword";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Change Password";
            this.pan_changePassword.ResumeLayout(false);
            this.pan_changePassword.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pan_changePassword;
        private System.Windows.Forms.Label lbl_changePassword;
        private System.Windows.Forms.Label lbl_userName;
        private System.Windows.Forms.TextBox txt_userName;
        private System.Windows.Forms.Label lbl_currentPassword;
        private System.Windows.Forms.Label lbl_newPassword;
        private System.Windows.Forms.TextBox txt_currentPassword;
        private System.Windows.Forms.Label lbl_confirmPassword;
        private System.Windows.Forms.TextBox txt_newPassword;
        private System.Windows.Forms.TextBox txt_confirmPassword;
        private System.Windows.Forms.Button btn_update;
        private System.Windows.Forms.Button btn_cancel;
    }
}