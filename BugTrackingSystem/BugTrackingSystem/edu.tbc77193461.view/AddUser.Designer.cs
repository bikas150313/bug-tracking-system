﻿namespace BugTrackingSystem
{
    partial class AddUser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddUser));
            this.pan_addUser = new System.Windows.Forms.Panel();
            this.btn_cancel = new System.Windows.Forms.Button();
            this.btn_add = new System.Windows.Forms.Button();
            this.txt_confirmPassword = new System.Windows.Forms.TextBox();
            this.lbl_confirmPassword = new System.Windows.Forms.Label();
            this.txt_password = new System.Windows.Forms.TextBox();
            this.lbl_password = new System.Windows.Forms.Label();
            this.combo_role = new System.Windows.Forms.ComboBox();
            this.lbl_role = new System.Windows.Forms.Label();
            this.txt_email = new System.Windows.Forms.TextBox();
            this.lbl_email = new System.Windows.Forms.Label();
            this.txt_contactNo = new System.Windows.Forms.TextBox();
            this.lbl_contactNo = new System.Windows.Forms.Label();
            this.txt_address = new System.Windows.Forms.TextBox();
            this.lbl_address = new System.Windows.Forms.Label();
            this.txt_userName = new System.Windows.Forms.TextBox();
            this.lbl_userName = new System.Windows.Forms.Label();
            this.txt_fullName = new System.Windows.Forms.TextBox();
            this.lbl_fullName = new System.Windows.Forms.Label();
            this.lbl_addUser = new System.Windows.Forms.Label();
            this.pan_addUser.SuspendLayout();
            this.SuspendLayout();
            // 
            // pan_addUser
            // 
            this.pan_addUser.BackColor = System.Drawing.Color.Black;
            this.pan_addUser.Controls.Add(this.btn_cancel);
            this.pan_addUser.Controls.Add(this.btn_add);
            this.pan_addUser.Controls.Add(this.txt_confirmPassword);
            this.pan_addUser.Controls.Add(this.lbl_confirmPassword);
            this.pan_addUser.Controls.Add(this.txt_password);
            this.pan_addUser.Controls.Add(this.lbl_password);
            this.pan_addUser.Controls.Add(this.combo_role);
            this.pan_addUser.Controls.Add(this.lbl_role);
            this.pan_addUser.Controls.Add(this.txt_email);
            this.pan_addUser.Controls.Add(this.lbl_email);
            this.pan_addUser.Controls.Add(this.txt_contactNo);
            this.pan_addUser.Controls.Add(this.lbl_contactNo);
            this.pan_addUser.Controls.Add(this.txt_address);
            this.pan_addUser.Controls.Add(this.lbl_address);
            this.pan_addUser.Controls.Add(this.txt_userName);
            this.pan_addUser.Controls.Add(this.lbl_userName);
            this.pan_addUser.Controls.Add(this.txt_fullName);
            this.pan_addUser.Controls.Add(this.lbl_fullName);
            this.pan_addUser.Controls.Add(this.lbl_addUser);
            this.pan_addUser.Location = new System.Drawing.Point(0, 0);
            this.pan_addUser.Name = "pan_addUser";
            this.pan_addUser.Size = new System.Drawing.Size(636, 568);
            this.pan_addUser.TabIndex = 0;
            // 
            // btn_cancel
            // 
            this.btn_cancel.Font = new System.Drawing.Font("Algerian", 15.75F);
            this.btn_cancel.ForeColor = System.Drawing.Color.Red;
            this.btn_cancel.Image = ((System.Drawing.Image)(resources.GetObject("btn_cancel.Image")));
            this.btn_cancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_cancel.Location = new System.Drawing.Point(439, 503);
            this.btn_cancel.Name = "btn_cancel";
            this.btn_cancel.Size = new System.Drawing.Size(118, 36);
            this.btn_cancel.TabIndex = 23;
            this.btn_cancel.Text = "Cancel";
            this.btn_cancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_cancel.UseVisualStyleBackColor = true;
            this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
            // 
            // btn_add
            // 
            this.btn_add.Font = new System.Drawing.Font("Algerian", 15.75F);
            this.btn_add.ForeColor = System.Drawing.Color.Green;
            this.btn_add.Image = ((System.Drawing.Image)(resources.GetObject("btn_add.Image")));
            this.btn_add.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_add.Location = new System.Drawing.Point(82, 503);
            this.btn_add.Name = "btn_add";
            this.btn_add.Size = new System.Drawing.Size(82, 36);
            this.btn_add.TabIndex = 22;
            this.btn_add.Text = "Add";
            this.btn_add.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_add.UseVisualStyleBackColor = true;
            this.btn_add.Click += new System.EventHandler(this.btn_add_Click);
            // 
            // txt_confirmPassword
            // 
            this.txt_confirmPassword.Font = new System.Drawing.Font("Consolas", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_confirmPassword.ForeColor = System.Drawing.Color.Blue;
            this.txt_confirmPassword.Location = new System.Drawing.Point(332, 421);
            this.txt_confirmPassword.Name = "txt_confirmPassword";
            this.txt_confirmPassword.Size = new System.Drawing.Size(292, 32);
            this.txt_confirmPassword.TabIndex = 21;
            this.txt_confirmPassword.UseSystemPasswordChar = true;
            // 
            // lbl_confirmPassword
            // 
            this.lbl_confirmPassword.AutoSize = true;
            this.lbl_confirmPassword.Font = new System.Drawing.Font("Hobo Std", 21.75F, System.Drawing.FontStyle.Bold);
            this.lbl_confirmPassword.ForeColor = System.Drawing.Color.MintCream;
            this.lbl_confirmPassword.Location = new System.Drawing.Point(12, 412);
            this.lbl_confirmPassword.Name = "lbl_confirmPassword";
            this.lbl_confirmPassword.Size = new System.Drawing.Size(291, 41);
            this.lbl_confirmPassword.TabIndex = 20;
            this.lbl_confirmPassword.Text = "Confirm Password :";
            // 
            // txt_password
            // 
            this.txt_password.Font = new System.Drawing.Font("Consolas", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_password.ForeColor = System.Drawing.Color.Blue;
            this.txt_password.Location = new System.Drawing.Point(332, 369);
            this.txt_password.Name = "txt_password";
            this.txt_password.Size = new System.Drawing.Size(292, 32);
            this.txt_password.TabIndex = 19;
            this.txt_password.UseSystemPasswordChar = true;
            // 
            // lbl_password
            // 
            this.lbl_password.AutoSize = true;
            this.lbl_password.Font = new System.Drawing.Font("Hobo Std", 21.75F, System.Drawing.FontStyle.Bold);
            this.lbl_password.ForeColor = System.Drawing.Color.MintCream;
            this.lbl_password.Location = new System.Drawing.Point(12, 360);
            this.lbl_password.Name = "lbl_password";
            this.lbl_password.Size = new System.Drawing.Size(172, 41);
            this.lbl_password.TabIndex = 18;
            this.lbl_password.Text = "Password :";
            // 
            // combo_role
            // 
            this.combo_role.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combo_role.Font = new System.Drawing.Font("Consolas", 15.75F);
            this.combo_role.FormattingEnabled = true;
            this.combo_role.Items.AddRange(new object[] {
            "Developer",
            "Tester"});
            this.combo_role.Location = new System.Drawing.Point(332, 318);
            this.combo_role.Name = "combo_role";
            this.combo_role.Size = new System.Drawing.Size(292, 32);
            this.combo_role.TabIndex = 17;
            // 
            // lbl_role
            // 
            this.lbl_role.AutoSize = true;
            this.lbl_role.Font = new System.Drawing.Font("Hobo Std", 21.75F, System.Drawing.FontStyle.Bold);
            this.lbl_role.ForeColor = System.Drawing.Color.MintCream;
            this.lbl_role.Location = new System.Drawing.Point(12, 308);
            this.lbl_role.Name = "lbl_role";
            this.lbl_role.Size = new System.Drawing.Size(97, 41);
            this.lbl_role.TabIndex = 16;
            this.lbl_role.Text = "Role :";
            // 
            // txt_email
            // 
            this.txt_email.Font = new System.Drawing.Font("Consolas", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_email.ForeColor = System.Drawing.Color.Blue;
            this.txt_email.Location = new System.Drawing.Point(332, 263);
            this.txt_email.Name = "txt_email";
            this.txt_email.Size = new System.Drawing.Size(292, 32);
            this.txt_email.TabIndex = 15;
            // 
            // lbl_email
            // 
            this.lbl_email.AutoSize = true;
            this.lbl_email.Font = new System.Drawing.Font("Hobo Std", 21.75F, System.Drawing.FontStyle.Bold);
            this.lbl_email.ForeColor = System.Drawing.Color.MintCream;
            this.lbl_email.Location = new System.Drawing.Point(12, 254);
            this.lbl_email.Name = "lbl_email";
            this.lbl_email.Size = new System.Drawing.Size(111, 41);
            this.lbl_email.TabIndex = 14;
            this.lbl_email.Text = "Email :";
            // 
            // txt_contactNo
            // 
            this.txt_contactNo.Font = new System.Drawing.Font("Consolas", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_contactNo.ForeColor = System.Drawing.Color.Blue;
            this.txt_contactNo.Location = new System.Drawing.Point(332, 210);
            this.txt_contactNo.Name = "txt_contactNo";
            this.txt_contactNo.Size = new System.Drawing.Size(292, 32);
            this.txt_contactNo.TabIndex = 13;
            // 
            // lbl_contactNo
            // 
            this.lbl_contactNo.AutoSize = true;
            this.lbl_contactNo.Font = new System.Drawing.Font("Hobo Std", 21.75F, System.Drawing.FontStyle.Bold);
            this.lbl_contactNo.ForeColor = System.Drawing.Color.MintCream;
            this.lbl_contactNo.Location = new System.Drawing.Point(12, 201);
            this.lbl_contactNo.Name = "lbl_contactNo";
            this.lbl_contactNo.Size = new System.Drawing.Size(259, 41);
            this.lbl_contactNo.TabIndex = 12;
            this.lbl_contactNo.Text = "Contact Number :";
            // 
            // txt_address
            // 
            this.txt_address.Font = new System.Drawing.Font("Consolas", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_address.ForeColor = System.Drawing.Color.Blue;
            this.txt_address.Location = new System.Drawing.Point(332, 159);
            this.txt_address.Name = "txt_address";
            this.txt_address.Size = new System.Drawing.Size(292, 32);
            this.txt_address.TabIndex = 11;
            // 
            // lbl_address
            // 
            this.lbl_address.AutoSize = true;
            this.lbl_address.Font = new System.Drawing.Font("Hobo Std", 21.75F, System.Drawing.FontStyle.Bold);
            this.lbl_address.ForeColor = System.Drawing.Color.MintCream;
            this.lbl_address.Location = new System.Drawing.Point(12, 150);
            this.lbl_address.Name = "lbl_address";
            this.lbl_address.Size = new System.Drawing.Size(152, 41);
            this.lbl_address.TabIndex = 10;
            this.lbl_address.Text = "Address :";
            // 
            // txt_userName
            // 
            this.txt_userName.Font = new System.Drawing.Font("Consolas", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_userName.ForeColor = System.Drawing.Color.Blue;
            this.txt_userName.Location = new System.Drawing.Point(332, 104);
            this.txt_userName.Name = "txt_userName";
            this.txt_userName.Size = new System.Drawing.Size(292, 32);
            this.txt_userName.TabIndex = 9;
            // 
            // lbl_userName
            // 
            this.lbl_userName.AutoSize = true;
            this.lbl_userName.Font = new System.Drawing.Font("Hobo Std", 21.75F, System.Drawing.FontStyle.Bold);
            this.lbl_userName.ForeColor = System.Drawing.Color.MintCream;
            this.lbl_userName.Location = new System.Drawing.Point(12, 94);
            this.lbl_userName.Name = "lbl_userName";
            this.lbl_userName.Size = new System.Drawing.Size(176, 41);
            this.lbl_userName.TabIndex = 8;
            this.lbl_userName.Text = "Username :";
            // 
            // txt_fullName
            // 
            this.txt_fullName.Font = new System.Drawing.Font("Consolas", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_fullName.ForeColor = System.Drawing.Color.Blue;
            this.txt_fullName.Location = new System.Drawing.Point(332, 53);
            this.txt_fullName.Name = "txt_fullName";
            this.txt_fullName.Size = new System.Drawing.Size(292, 32);
            this.txt_fullName.TabIndex = 7;
            // 
            // lbl_fullName
            // 
            this.lbl_fullName.AutoSize = true;
            this.lbl_fullName.Font = new System.Drawing.Font("Hobo Std", 21.75F, System.Drawing.FontStyle.Bold);
            this.lbl_fullName.ForeColor = System.Drawing.Color.MintCream;
            this.lbl_fullName.Location = new System.Drawing.Point(12, 44);
            this.lbl_fullName.Name = "lbl_fullName";
            this.lbl_fullName.Size = new System.Drawing.Size(173, 41);
            this.lbl_fullName.TabIndex = 6;
            this.lbl_fullName.Text = "Full Name :";
            // 
            // lbl_addUser
            // 
            this.lbl_addUser.AutoSize = true;
            this.lbl_addUser.Font = new System.Drawing.Font("Cooper Black", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_addUser.ForeColor = System.Drawing.Color.Aqua;
            this.lbl_addUser.Location = new System.Drawing.Point(255, 9);
            this.lbl_addUser.Name = "lbl_addUser";
            this.lbl_addUser.Size = new System.Drawing.Size(153, 34);
            this.lbl_addUser.TabIndex = 5;
            this.lbl_addUser.Text = "Add User";
            // 
            // AddUser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(636, 567);
            this.Controls.Add(this.pan_addUser);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AddUser";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Add User";
            this.pan_addUser.ResumeLayout(false);
            this.pan_addUser.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pan_addUser;
        private System.Windows.Forms.Label lbl_addUser;
        private System.Windows.Forms.Label lbl_fullName;
        private System.Windows.Forms.TextBox txt_fullName;
        private System.Windows.Forms.TextBox txt_email;
        private System.Windows.Forms.Label lbl_email;
        private System.Windows.Forms.TextBox txt_contactNo;
        private System.Windows.Forms.Label lbl_contactNo;
        private System.Windows.Forms.TextBox txt_address;
        private System.Windows.Forms.Label lbl_address;
        private System.Windows.Forms.TextBox txt_userName;
        private System.Windows.Forms.Label lbl_userName;
        private System.Windows.Forms.Label lbl_role;
        private System.Windows.Forms.ComboBox combo_role;
        private System.Windows.Forms.Label lbl_password;
        private System.Windows.Forms.Label lbl_confirmPassword;
        private System.Windows.Forms.TextBox txt_password;
        private System.Windows.Forms.TextBox txt_confirmPassword;
        private System.Windows.Forms.Button btn_add;
        private System.Windows.Forms.Button btn_cancel;
    }
}