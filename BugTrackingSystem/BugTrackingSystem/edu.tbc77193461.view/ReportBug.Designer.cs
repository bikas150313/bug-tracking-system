﻿namespace BugTrackingSystem.edu.tbc77193461.view
{
    partial class ReportBug
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReportBug));
            this.pan_reportBug = new System.Windows.Forms.Panel();
            this.lbl_fileName = new System.Windows.Forms.Label();
            this.btn_browse = new System.Windows.Forms.Button();
            this.lbl_screenshot = new System.Windows.Forms.Label();
            this.combo_year = new System.Windows.Forms.ComboBox();
            this.combo_date = new System.Windows.Forms.ComboBox();
            this.combo_month = new System.Windows.Forms.ComboBox();
            this.lbl_date = new System.Windows.Forms.Label();
            this.txt_method = new System.Windows.Forms.TextBox();
            this.btn_cancel = new System.Windows.Forms.Button();
            this.btn_report = new System.Windows.Forms.Button();
            this.txt_codeAuthor = new System.Windows.Forms.TextBox();
            this.lbl_codeAuthor = new System.Windows.Forms.Label();
            this.txt_lineNumber = new System.Windows.Forms.TextBox();
            this.lbl_lineNumber = new System.Windows.Forms.Label();
            this.lbl_method = new System.Windows.Forms.Label();
            this.txt_class = new System.Windows.Forms.TextBox();
            this.lbl_class = new System.Windows.Forms.Label();
            this.txt_project = new System.Windows.Forms.TextBox();
            this.lbl_project = new System.Windows.Forms.Label();
            this.lbl_bugDescription = new System.Windows.Forms.Label();
            this.lbl_reportBug = new System.Windows.Forms.Label();
            this.txt_bugDescription = new System.Windows.Forms.RichTextBox();
            this.pan_reportBug.SuspendLayout();
            this.SuspendLayout();
            // 
            // pan_reportBug
            // 
            this.pan_reportBug.BackColor = System.Drawing.Color.Black;
            this.pan_reportBug.Controls.Add(this.txt_bugDescription);
            this.pan_reportBug.Controls.Add(this.lbl_fileName);
            this.pan_reportBug.Controls.Add(this.btn_browse);
            this.pan_reportBug.Controls.Add(this.lbl_screenshot);
            this.pan_reportBug.Controls.Add(this.combo_year);
            this.pan_reportBug.Controls.Add(this.combo_date);
            this.pan_reportBug.Controls.Add(this.combo_month);
            this.pan_reportBug.Controls.Add(this.lbl_date);
            this.pan_reportBug.Controls.Add(this.txt_method);
            this.pan_reportBug.Controls.Add(this.btn_cancel);
            this.pan_reportBug.Controls.Add(this.btn_report);
            this.pan_reportBug.Controls.Add(this.txt_codeAuthor);
            this.pan_reportBug.Controls.Add(this.lbl_codeAuthor);
            this.pan_reportBug.Controls.Add(this.txt_lineNumber);
            this.pan_reportBug.Controls.Add(this.lbl_lineNumber);
            this.pan_reportBug.Controls.Add(this.lbl_method);
            this.pan_reportBug.Controls.Add(this.txt_class);
            this.pan_reportBug.Controls.Add(this.lbl_class);
            this.pan_reportBug.Controls.Add(this.txt_project);
            this.pan_reportBug.Controls.Add(this.lbl_project);
            this.pan_reportBug.Controls.Add(this.lbl_bugDescription);
            this.pan_reportBug.Controls.Add(this.lbl_reportBug);
            this.pan_reportBug.Location = new System.Drawing.Point(0, -1);
            this.pan_reportBug.Name = "pan_reportBug";
            this.pan_reportBug.Size = new System.Drawing.Size(636, 666);
            this.pan_reportBug.TabIndex = 1;
            // 
            // lbl_fileName
            // 
            this.lbl_fileName.AutoSize = true;
            this.lbl_fileName.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_fileName.ForeColor = System.Drawing.Color.White;
            this.lbl_fileName.Location = new System.Drawing.Point(329, 582);
            this.lbl_fileName.Name = "lbl_fileName";
            this.lbl_fileName.Size = new System.Drawing.Size(0, 13);
            this.lbl_fileName.TabIndex = 31;
            // 
            // btn_browse
            // 
            this.btn_browse.Font = new System.Drawing.Font("Algerian", 15.75F);
            this.btn_browse.ForeColor = System.Drawing.Color.Blue;
            this.btn_browse.Image = ((System.Drawing.Image)(resources.GetObject("btn_browse.Image")));
            this.btn_browse.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_browse.Location = new System.Drawing.Point(332, 528);
            this.btn_browse.Name = "btn_browse";
            this.btn_browse.Size = new System.Drawing.Size(123, 36);
            this.btn_browse.TabIndex = 30;
            this.btn_browse.Text = "Browse";
            this.btn_browse.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_browse.UseVisualStyleBackColor = true;
            this.btn_browse.Click += new System.EventHandler(this.btn_browse_Click);
            // 
            // lbl_screenshot
            // 
            this.lbl_screenshot.AutoSize = true;
            this.lbl_screenshot.Font = new System.Drawing.Font("Hobo Std", 21.75F, System.Drawing.FontStyle.Bold);
            this.lbl_screenshot.ForeColor = System.Drawing.Color.MintCream;
            this.lbl_screenshot.Location = new System.Drawing.Point(13, 523);
            this.lbl_screenshot.Name = "lbl_screenshot";
            this.lbl_screenshot.Size = new System.Drawing.Size(214, 41);
            this.lbl_screenshot.TabIndex = 29;
            this.lbl_screenshot.Text = "Screenshot* :";
            // 
            // combo_year
            // 
            this.combo_year.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combo_year.Font = new System.Drawing.Font("Consolas", 15.75F);
            this.combo_year.FormattingEnabled = true;
            this.combo_year.Items.AddRange(new object[] {
            "2018",
            "2019",
            "2020",
            "2021",
            "2022",
            "2023",
            "2024",
            "2025",
            "2026",
            "2027",
            "2028",
            "2029",
            "2030"});
            this.combo_year.Location = new System.Drawing.Point(538, 477);
            this.combo_year.Name = "combo_year";
            this.combo_year.Size = new System.Drawing.Size(86, 32);
            this.combo_year.TabIndex = 25;
            // 
            // combo_date
            // 
            this.combo_date.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combo_date.Font = new System.Drawing.Font("Consolas", 15.75F);
            this.combo_date.FormattingEnabled = true;
            this.combo_date.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24",
            "25",
            "26",
            "27",
            "28",
            "29",
            "30",
            "31",
            "32"});
            this.combo_date.Location = new System.Drawing.Point(434, 477);
            this.combo_date.Name = "combo_date";
            this.combo_date.Size = new System.Drawing.Size(86, 32);
            this.combo_date.TabIndex = 24;
            // 
            // combo_month
            // 
            this.combo_month.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combo_month.Font = new System.Drawing.Font("Consolas", 15.75F);
            this.combo_month.FormattingEnabled = true;
            this.combo_month.Items.AddRange(new object[] {
            "JAN",
            "FEB",
            "MAR",
            "APR",
            "MAY",
            "JUN",
            "JUL",
            "AUG",
            "SEP",
            "OCT",
            "NOV",
            "DEC"});
            this.combo_month.Location = new System.Drawing.Point(332, 477);
            this.combo_month.Name = "combo_month";
            this.combo_month.Size = new System.Drawing.Size(86, 32);
            this.combo_month.TabIndex = 23;
            // 
            // lbl_date
            // 
            this.lbl_date.AutoSize = true;
            this.lbl_date.Font = new System.Drawing.Font("Hobo Std", 21.75F, System.Drawing.FontStyle.Bold);
            this.lbl_date.ForeColor = System.Drawing.Color.MintCream;
            this.lbl_date.Location = new System.Drawing.Point(13, 468);
            this.lbl_date.Name = "lbl_date";
            this.lbl_date.Size = new System.Drawing.Size(119, 41);
            this.lbl_date.TabIndex = 22;
            this.lbl_date.Text = "Date* :";
            // 
            // txt_method
            // 
            this.txt_method.Font = new System.Drawing.Font("Consolas", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_method.ForeColor = System.Drawing.Color.Black;
            this.txt_method.Location = new System.Drawing.Point(332, 317);
            this.txt_method.Name = "txt_method";
            this.txt_method.Size = new System.Drawing.Size(292, 32);
            this.txt_method.TabIndex = 17;
            // 
            // btn_cancel
            // 
            this.btn_cancel.Font = new System.Drawing.Font("Algerian", 15.75F);
            this.btn_cancel.ForeColor = System.Drawing.Color.Red;
            this.btn_cancel.Image = ((System.Drawing.Image)(resources.GetObject("btn_cancel.Image")));
            this.btn_cancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_cancel.Location = new System.Drawing.Point(446, 618);
            this.btn_cancel.Name = "btn_cancel";
            this.btn_cancel.Size = new System.Drawing.Size(118, 36);
            this.btn_cancel.TabIndex = 33;
            this.btn_cancel.Text = "Cancel";
            this.btn_cancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_cancel.UseVisualStyleBackColor = true;
            this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
            // 
            // btn_report
            // 
            this.btn_report.Font = new System.Drawing.Font("Algerian", 15.75F);
            this.btn_report.ForeColor = System.Drawing.Color.Green;
            this.btn_report.Image = ((System.Drawing.Image)(resources.GetObject("btn_report.Image")));
            this.btn_report.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_report.Location = new System.Drawing.Point(70, 618);
            this.btn_report.Name = "btn_report";
            this.btn_report.Size = new System.Drawing.Size(123, 36);
            this.btn_report.TabIndex = 32;
            this.btn_report.Text = "Report";
            this.btn_report.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_report.UseVisualStyleBackColor = true;
            this.btn_report.Click += new System.EventHandler(this.btn_report_Click);
            // 
            // txt_codeAuthor
            // 
            this.txt_codeAuthor.Font = new System.Drawing.Font("Consolas", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_codeAuthor.ForeColor = System.Drawing.Color.Black;
            this.txt_codeAuthor.Location = new System.Drawing.Point(332, 421);
            this.txt_codeAuthor.Name = "txt_codeAuthor";
            this.txt_codeAuthor.Size = new System.Drawing.Size(292, 32);
            this.txt_codeAuthor.TabIndex = 21;
            // 
            // lbl_codeAuthor
            // 
            this.lbl_codeAuthor.AutoSize = true;
            this.lbl_codeAuthor.Font = new System.Drawing.Font("Hobo Std", 21.75F, System.Drawing.FontStyle.Bold);
            this.lbl_codeAuthor.ForeColor = System.Drawing.Color.MintCream;
            this.lbl_codeAuthor.Location = new System.Drawing.Point(12, 412);
            this.lbl_codeAuthor.Name = "lbl_codeAuthor";
            this.lbl_codeAuthor.Size = new System.Drawing.Size(210, 41);
            this.lbl_codeAuthor.TabIndex = 20;
            this.lbl_codeAuthor.Text = "Code Author :";
            // 
            // txt_lineNumber
            // 
            this.txt_lineNumber.Font = new System.Drawing.Font("Consolas", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_lineNumber.ForeColor = System.Drawing.Color.Black;
            this.txt_lineNumber.Location = new System.Drawing.Point(332, 369);
            this.txt_lineNumber.Name = "txt_lineNumber";
            this.txt_lineNumber.Size = new System.Drawing.Size(292, 32);
            this.txt_lineNumber.TabIndex = 19;
            // 
            // lbl_lineNumber
            // 
            this.lbl_lineNumber.AutoSize = true;
            this.lbl_lineNumber.Font = new System.Drawing.Font("Hobo Std", 21.75F, System.Drawing.FontStyle.Bold);
            this.lbl_lineNumber.ForeColor = System.Drawing.Color.MintCream;
            this.lbl_lineNumber.Location = new System.Drawing.Point(12, 360);
            this.lbl_lineNumber.Name = "lbl_lineNumber";
            this.lbl_lineNumber.Size = new System.Drawing.Size(211, 41);
            this.lbl_lineNumber.TabIndex = 18;
            this.lbl_lineNumber.Text = "Line Number :";
            // 
            // lbl_method
            // 
            this.lbl_method.AutoSize = true;
            this.lbl_method.Font = new System.Drawing.Font("Hobo Std", 21.75F, System.Drawing.FontStyle.Bold);
            this.lbl_method.ForeColor = System.Drawing.Color.MintCream;
            this.lbl_method.Location = new System.Drawing.Point(12, 308);
            this.lbl_method.Name = "lbl_method";
            this.lbl_method.Size = new System.Drawing.Size(140, 41);
            this.lbl_method.TabIndex = 16;
            this.lbl_method.Text = "Method :";
            // 
            // txt_class
            // 
            this.txt_class.Font = new System.Drawing.Font("Consolas", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_class.ForeColor = System.Drawing.Color.Black;
            this.txt_class.Location = new System.Drawing.Point(332, 263);
            this.txt_class.Name = "txt_class";
            this.txt_class.Size = new System.Drawing.Size(292, 32);
            this.txt_class.TabIndex = 15;
            // 
            // lbl_class
            // 
            this.lbl_class.AutoSize = true;
            this.lbl_class.Font = new System.Drawing.Font("Hobo Std", 21.75F, System.Drawing.FontStyle.Bold);
            this.lbl_class.ForeColor = System.Drawing.Color.MintCream;
            this.lbl_class.Location = new System.Drawing.Point(12, 254);
            this.lbl_class.Name = "lbl_class";
            this.lbl_class.Size = new System.Drawing.Size(111, 41);
            this.lbl_class.TabIndex = 14;
            this.lbl_class.Text = "Class :";
            // 
            // txt_project
            // 
            this.txt_project.Font = new System.Drawing.Font("Consolas", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_project.ForeColor = System.Drawing.Color.Black;
            this.txt_project.Location = new System.Drawing.Point(332, 210);
            this.txt_project.Name = "txt_project";
            this.txt_project.Size = new System.Drawing.Size(292, 32);
            this.txt_project.TabIndex = 13;
            // 
            // lbl_project
            // 
            this.lbl_project.AutoSize = true;
            this.lbl_project.Font = new System.Drawing.Font("Hobo Std", 21.75F, System.Drawing.FontStyle.Bold);
            this.lbl_project.ForeColor = System.Drawing.Color.MintCream;
            this.lbl_project.Location = new System.Drawing.Point(12, 201);
            this.lbl_project.Name = "lbl_project";
            this.lbl_project.Size = new System.Drawing.Size(157, 41);
            this.lbl_project.TabIndex = 12;
            this.lbl_project.Text = "Project* :";
            // 
            // lbl_bugDescription
            // 
            this.lbl_bugDescription.AutoSize = true;
            this.lbl_bugDescription.Font = new System.Drawing.Font("Hobo Std", 21.75F, System.Drawing.FontStyle.Bold);
            this.lbl_bugDescription.ForeColor = System.Drawing.Color.MintCream;
            this.lbl_bugDescription.Location = new System.Drawing.Point(12, 44);
            this.lbl_bugDescription.Name = "lbl_bugDescription";
            this.lbl_bugDescription.Size = new System.Drawing.Size(276, 41);
            this.lbl_bugDescription.TabIndex = 6;
            this.lbl_bugDescription.Text = "Bug Description* :";
            // 
            // lbl_reportBug
            // 
            this.lbl_reportBug.AutoSize = true;
            this.lbl_reportBug.Font = new System.Drawing.Font("Cooper Black", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_reportBug.ForeColor = System.Drawing.Color.Aqua;
            this.lbl_reportBug.Location = new System.Drawing.Point(235, 10);
            this.lbl_reportBug.Name = "lbl_reportBug";
            this.lbl_reportBug.Size = new System.Drawing.Size(183, 34);
            this.lbl_reportBug.TabIndex = 5;
            this.lbl_reportBug.Text = "Report Bug";
            // 
            // txt_bugDescription
            // 
            this.txt_bugDescription.Font = new System.Drawing.Font("Consolas", 15.75F);
            this.txt_bugDescription.Location = new System.Drawing.Point(332, 62);
            this.txt_bugDescription.Name = "txt_bugDescription";
            this.txt_bugDescription.Size = new System.Drawing.Size(292, 133);
            this.txt_bugDescription.TabIndex = 34;
            this.txt_bugDescription.Text = "";
            this.txt_bugDescription.TextChanged += new System.EventHandler(this.txt_bugDescription_TextChanged);
            // 
            // ReportBug
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(636, 665);
            this.Controls.Add(this.pan_reportBug);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ReportBug";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Report Bug";
            this.pan_reportBug.ResumeLayout(false);
            this.pan_reportBug.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pan_reportBug;
        private System.Windows.Forms.Button btn_cancel;
        private System.Windows.Forms.Button btn_report;
        private System.Windows.Forms.TextBox txt_codeAuthor;
        private System.Windows.Forms.Label lbl_codeAuthor;
        private System.Windows.Forms.TextBox txt_lineNumber;
        private System.Windows.Forms.Label lbl_lineNumber;
        private System.Windows.Forms.Label lbl_method;
        private System.Windows.Forms.TextBox txt_class;
        private System.Windows.Forms.Label lbl_class;
        private System.Windows.Forms.TextBox txt_project;
        private System.Windows.Forms.Label lbl_project;
        private System.Windows.Forms.Label lbl_bugDescription;
        private System.Windows.Forms.Label lbl_reportBug;
        private System.Windows.Forms.TextBox txt_method;
        private System.Windows.Forms.Label lbl_date;
        private System.Windows.Forms.ComboBox combo_month;
        private System.Windows.Forms.ComboBox combo_year;
        private System.Windows.Forms.ComboBox combo_date;
        private System.Windows.Forms.Label lbl_screenshot;
        private System.Windows.Forms.Button btn_browse;
        private System.Windows.Forms.Label lbl_fileName;
        private System.Windows.Forms.RichTextBox txt_bugDescription;
    }
}