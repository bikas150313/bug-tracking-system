﻿namespace BugTrackingSystem.edu.tbc77193461.view
{
    partial class AdminDashboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AdminDashboard));
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.fileMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem_changePassword = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem_logout = new System.Windows.Forms.ToolStripMenuItem();
            this.userMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem_addUser = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem_viewUser = new System.Windows.Forms.ToolStripMenuItem();
            this.versionControlMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem_connectToBitbucket = new System.Windows.Forms.ToolStripMenuItem();
            this.pan_dashboard = new System.Windows.Forms.Panel();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.lbl_bugTrackingSystem = new System.Windows.Forms.Label();
            this.helpMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem_about = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip.SuspendLayout();
            this.pan_dashboard.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileMenu,
            this.userMenu,
            this.versionControlMenu,
            this.helpMenu});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(735, 30);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "adminMenuStrip";
            // 
            // fileMenu
            // 
            this.fileMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuItem_changePassword,
            this.menuItem_logout});
            this.fileMenu.Font = new System.Drawing.Font("Consolas", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fileMenu.Name = "fileMenu";
            this.fileMenu.Size = new System.Drawing.Size(62, 26);
            this.fileMenu.Text = "File";
            // 
            // menuItem_changePassword
            // 
            this.menuItem_changePassword.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuItem_changePassword.Image = ((System.Drawing.Image)(resources.GetObject("menuItem_changePassword.Image")));
            this.menuItem_changePassword.Name = "menuItem_changePassword";
            this.menuItem_changePassword.Size = new System.Drawing.Size(214, 24);
            this.menuItem_changePassword.Text = "Change Password";
            this.menuItem_changePassword.Click += new System.EventHandler(this.menuItem_changePassword_Click);
            // 
            // menuItem_logout
            // 
            this.menuItem_logout.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuItem_logout.Image = ((System.Drawing.Image)(resources.GetObject("menuItem_logout.Image")));
            this.menuItem_logout.Name = "menuItem_logout";
            this.menuItem_logout.Size = new System.Drawing.Size(214, 24);
            this.menuItem_logout.Text = "Logout";
            this.menuItem_logout.Click += new System.EventHandler(this.menuItem_logout_Click);
            // 
            // userMenu
            // 
            this.userMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuItem_addUser,
            this.menuItem_viewUser});
            this.userMenu.Font = new System.Drawing.Font("Consolas", 14.25F);
            this.userMenu.Name = "userMenu";
            this.userMenu.Size = new System.Drawing.Size(62, 26);
            this.userMenu.Text = "User";
            // 
            // menuItem_addUser
            // 
            this.menuItem_addUser.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuItem_addUser.Image = ((System.Drawing.Image)(resources.GetObject("menuItem_addUser.Image")));
            this.menuItem_addUser.Name = "menuItem_addUser";
            this.menuItem_addUser.Size = new System.Drawing.Size(205, 24);
            this.menuItem_addUser.Text = "Add User";
            this.menuItem_addUser.Click += new System.EventHandler(this.menuItem_addUser_Click);
            // 
            // menuItem_viewUser
            // 
            this.menuItem_viewUser.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuItem_viewUser.Image = ((System.Drawing.Image)(resources.GetObject("menuItem_viewUser.Image")));
            this.menuItem_viewUser.Name = "menuItem_viewUser";
            this.menuItem_viewUser.Size = new System.Drawing.Size(205, 24);
            this.menuItem_viewUser.Text = "View All Users";
            this.menuItem_viewUser.Click += new System.EventHandler(this.menuItem_viewUser_Click);
            // 
            // versionControlMenu
            // 
            this.versionControlMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuItem_connectToBitbucket});
            this.versionControlMenu.Font = new System.Drawing.Font("Consolas", 14.25F);
            this.versionControlMenu.Name = "versionControlMenu";
            this.versionControlMenu.Size = new System.Drawing.Size(172, 26);
            this.versionControlMenu.Text = "Version Control";
            // 
            // menuItem_connectToBitbucket
            // 
            this.menuItem_connectToBitbucket.Font = new System.Drawing.Font("Consolas", 12F);
            this.menuItem_connectToBitbucket.Image = ((System.Drawing.Image)(resources.GetObject("menuItem_connectToBitbucket.Image")));
            this.menuItem_connectToBitbucket.Name = "menuItem_connectToBitbucket";
            this.menuItem_connectToBitbucket.Size = new System.Drawing.Size(259, 24);
            this.menuItem_connectToBitbucket.Text = "Connect to Bitbucket";
            this.menuItem_connectToBitbucket.Click += new System.EventHandler(this.menuItem_connectToBitbucket_Click);
            // 
            // pan_dashboard
            // 
            this.pan_dashboard.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pan_dashboard.BackColor = System.Drawing.Color.Black;
            this.pan_dashboard.Controls.Add(this.pictureBox);
            this.pan_dashboard.Controls.Add(this.lbl_bugTrackingSystem);
            this.pan_dashboard.Location = new System.Drawing.Point(0, 33);
            this.pan_dashboard.Name = "pan_dashboard";
            this.pan_dashboard.Size = new System.Drawing.Size(735, 471);
            this.pan_dashboard.TabIndex = 2;
            // 
            // pictureBox
            // 
            this.pictureBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox.Image")));
            this.pictureBox.Location = new System.Drawing.Point(219, 104);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(338, 240);
            this.pictureBox.TabIndex = 1;
            this.pictureBox.TabStop = false;
            // 
            // lbl_bugTrackingSystem
            // 
            this.lbl_bugTrackingSystem.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_bugTrackingSystem.Font = new System.Drawing.Font("Impact", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_bugTrackingSystem.ForeColor = System.Drawing.Color.White;
            this.lbl_bugTrackingSystem.Location = new System.Drawing.Point(28, 10);
            this.lbl_bugTrackingSystem.Name = "lbl_bugTrackingSystem";
            this.lbl_bugTrackingSystem.Size = new System.Drawing.Size(695, 91);
            this.lbl_bugTrackingSystem.TabIndex = 0;
            this.lbl_bugTrackingSystem.Text = "Bug Tracking System";
            this.lbl_bugTrackingSystem.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // helpMenu
            // 
            this.helpMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuItem_about});
            this.helpMenu.Font = new System.Drawing.Font("Consolas", 14.25F);
            this.helpMenu.Name = "helpMenu";
            this.helpMenu.Size = new System.Drawing.Size(62, 26);
            this.helpMenu.Text = "Help";
            // 
            // menuItem_about
            // 
            this.menuItem_about.Font = new System.Drawing.Font("Consolas", 12F);
            this.menuItem_about.Image = ((System.Drawing.Image)(resources.GetObject("menuItem_about.Image")));
            this.menuItem_about.Name = "menuItem_about";
            this.menuItem_about.Size = new System.Drawing.Size(152, 24);
            this.menuItem_about.Text = "About";
            this.menuItem_about.Click += new System.EventHandler(this.menuItem_about_Click);
            // 
            // AdminDashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(735, 506);
            this.Controls.Add(this.pan_dashboard);
            this.Controls.Add(this.menuStrip);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip;
            this.Name = "AdminDashboard";
            this.Text = "Admin Dashboard";
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.pan_dashboard.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem fileMenu;
        private System.Windows.Forms.ToolStripMenuItem menuItem_changePassword;
        private System.Windows.Forms.ToolStripMenuItem menuItem_logout;
        private System.Windows.Forms.ToolStripMenuItem userMenu;
        private System.Windows.Forms.ToolStripMenuItem menuItem_addUser;
        private System.Windows.Forms.ToolStripMenuItem menuItem_viewUser;
        private System.Windows.Forms.Panel pan_dashboard;
        private System.Windows.Forms.Label lbl_bugTrackingSystem;
        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.ToolStripMenuItem versionControlMenu;
        private System.Windows.Forms.ToolStripMenuItem menuItem_connectToBitbucket;
        private System.Windows.Forms.ToolStripMenuItem helpMenu;
        private System.Windows.Forms.ToolStripMenuItem menuItem_about;
    }
}