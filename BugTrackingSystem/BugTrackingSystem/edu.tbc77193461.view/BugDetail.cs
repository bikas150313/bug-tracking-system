﻿using BugTrackingSystem.edu.tbc77193461.controller;
using BugTrackingSystem.edu.tbc77193461.model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BugTrackingSystem.edu.tbc77193461.view
{
    public partial class BugDetail : Form
    {

        /// <summary>
        /// declaring required variable
        /// </summary>
        private int bugId;

        public BugDetail(int bugId, string bugDescription, string project, string className, string method,
            string lineNo, string author, MemoryStream memoryStream, string reportDate, string status, string fixDate,
            string fixer)
        {
            this.bugId = bugId;
            InitializeComponent();
            //initializing and modifying form component   
            if (status=="Fixed")
            {
                txt_bugDescription.ReadOnly = true;
                txt_project.ReadOnly = true;
                txt_class.ReadOnly = true;
                txt_method.ReadOnly = true;
                txt_lineNumber.ReadOnly = true;
                txt_codeAuthor.ReadOnly = true;
                btn_update.Visible = false;
                btn_cancel.Visible = false;
            }
            pictureBox_bug.SizeMode = PictureBoxSizeMode.AutoSize;
            pictureBox_bug.Image = Image.FromStream(memoryStream);
            txt_bugDescription.Text = bugDescription;
            txt_project.Text = project;
            txt_class.Text = className;
            txt_method.Text = method;
            txt_lineNumber.Text = lineNo;
            txt_codeAuthor.Text = author;
            txt_status.Text = status;
            txt_reportedDate.Text = reportDate;
            txt_associateDeveloper.Text = fixer;
            txt_markedAs.Text = status;
            txt_fixedDate.Text = fixDate;
        }

        public BugDetail(int bugId, string bugDescription, string project, string className, string method,
            string lineNo, string author, MemoryStream memoryStream, string reportDate, string status, string fixDate,
            string fixer, string markedAs)
        {
            this.bugId = bugId;
            InitializeComponent();
            //initializing form component
            pictureBox_bug.SizeMode = PictureBoxSizeMode.AutoSize;
            pictureBox_bug.Image = Image.FromStream(memoryStream);
            txt_bugDescription.Text = bugDescription;
            txt_project.Text = project;
            txt_class.Text = className;
            txt_method.Text = method;
            txt_lineNumber.Text = lineNo;
            txt_codeAuthor.Text = author;
            txt_status.Text = status;
            txt_reportedDate.Text = reportDate;
            txt_associateDeveloper.Text = fixer;
            txt_markedAs.Text = markedAs;
            txt_fixedDate.Text = fixDate;
        }

        /// <summary>
        /// event triggered on update button click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_update_Click(object sender, EventArgs e)
        {
            string bugDescription = txt_bugDescription.Text;
            string project = txt_project.Text;
            string className = txt_class.Text;
            string method = txt_method.Text;
            string lineNo = txt_lineNumber.Text;
            string codeAuthor = txt_codeAuthor.Text;
            if (!string.IsNullOrEmpty(bugDescription))
            {
                if (!string.IsNullOrEmpty(project))
                {
                    Bug bug = new Bug();
                    bug.BugId = bugId;
                    bug.BugDescription = bugDescription;
                    bug.Project = project;
                    bug.ClassName = className;
                    bug.Method = method;
                    bug.LineNumber = lineNo;
                    bug.Author = codeAuthor;
                    BugController bugController = new BugController();
                    Boolean updated = bugController.UpdateBug(bug);
                    if (updated)
                    {
                        MessageBox.Show("Bug details updated!");
                        this.Dispose();
                    }
                    else
                    {
                        MessageBox.Show("Unable to update bug details!");
                    }
                }
                else
                {
                    MessageBox.Show("Project name cannot be blank!");
                    txt_project.Focus();
                }
            }
            else
            {
                MessageBox.Show("Bug description cannot be blank!");
                txt_bugDescription.Focus();
            }
        }

        /// <summary>
        /// event triggered on cancel button click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_cancel_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        /// <summary>
        /// event triggered on text changed on rich text box
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_bugDescription_TextChanged(object sender, EventArgs e)
        {
            // getting keywords/functions
            string keywords = @"\b(public|private|partial|static|namespace|class|using
                            |void|foreach|in|int|string|this|new|try|catch|if|else|)\b";
            MatchCollection keywordMatches = Regex.Matches(txt_bugDescription.Text, keywords);

            //getting types/classes from the text 
            string types = @"\b(Console|MessageBox)\b";
            MatchCollection typeMatches = Regex.Matches(txt_bugDescription.Text, types);

            //getting comments (inline or multiline)
            string comments = @"(\/\/.+?$|\/\*.+?\*\/)";
            MatchCollection commentMatches = Regex.Matches(txt_bugDescription.Text, comments, RegexOptions.Multiline);

            //getting strings
            string strings = "\".+?\"";
            MatchCollection stringMatches = Regex.Matches(txt_bugDescription.Text, strings);

            // saving the original caret position + forecolor
            int originalIndex = txt_bugDescription.SelectionStart;
            int originalLength = txt_bugDescription.SelectionLength;
            Color originalColor = Color.Black;

            // MANDATORY - focuses a label before highlighting (avoids blinking)
            lbl_bugDescription.Focus();

            // removes any previous highlighting (so modified words won't remain highlighted)
            txt_bugDescription.SelectionStart = 0;
            txt_bugDescription.SelectionLength = txt_bugDescription.Text.Length;
            txt_bugDescription.SelectionColor = originalColor;

            // scanning...
            foreach (Match match in keywordMatches)
            {
                txt_bugDescription.SelectionStart = match.Index;
                txt_bugDescription.SelectionLength = match.Length;
                txt_bugDescription.SelectionColor = Color.Blue;
            }

            foreach (Match match in typeMatches)
            {
                txt_bugDescription.SelectionStart = match.Index;
                txt_bugDescription.SelectionLength = match.Length;
                txt_bugDescription.SelectionColor = Color.DarkCyan;
            }

            foreach (Match match in commentMatches)
            {
                txt_bugDescription.SelectionStart = match.Index;
                txt_bugDescription.SelectionLength = match.Length;
                txt_bugDescription.SelectionColor = Color.Green;
            }

            foreach (Match match in stringMatches)
            {
                txt_bugDescription.SelectionStart = match.Index;
                txt_bugDescription.SelectionLength = match.Length;
                txt_bugDescription.SelectionColor = Color.Brown;
            }

            // restoring the original colors, for further writing
            txt_bugDescription.SelectionStart = originalIndex;
            txt_bugDescription.SelectionLength = originalLength;
            txt_bugDescription.SelectionColor = originalColor;

            // giving back the focus
            txt_bugDescription.Focus();
        }
    }
}
