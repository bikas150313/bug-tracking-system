﻿using BugTrackingSystem.edu.tbc77193461.controller;
using BugTrackingSystem.edu.tbc77193461.model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BugTrackingSystem.edu.tbc77193461.view
{
    public partial class ActiveBugDetail : Form
    {

        /// <summary>
        /// declaring required variables
        /// </summary>
        private int bugId;
        private int userId;

        public ActiveBugDetail(int userId, int bugId, string bugDescription, string project, string className, string method,
            string lineNo, string author, MemoryStream memoryStream, string reportDate, string reporter)
        {
            this.userId = userId;
            this.bugId = bugId;
            InitializeComponent();
            //initializing form components
            pictureBox_bug.SizeMode = PictureBoxSizeMode.AutoSize;
            pictureBox_bug.Image = Image.FromStream(memoryStream);
            txt_bugDescription.Text = bugDescription;
            txt_project.Text = project;
            txt_class.Text = className;
            txt_method.Text = method;
            txt_lineNumber.Text = lineNo;
            txt_codeAuthor.Text = author;
            txt_reportedBy.Text = reporter;
            txt_reportedDate.Text = reportDate;
        }

        /// <summary>
        /// event triggered on cancel button click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_cancel_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        /// <summary>
        /// event triggered on update button click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_update_Click(object sender, EventArgs e)
        {
            string markAs = combo_markAs.Text;
            string month = combo_month.Text;
            string date = combo_date.Text;
            string year = combo_year.Text;
            if (!string.IsNullOrEmpty(markAs))
            {
                //if status is "Fixing"
                if (markAs=="Fixing")
                {
                    Bug bug = new Bug();
                    bug.FixerId = userId;
                    bug.BugId = bugId;
                    bug.Status = markAs;
                    BugController bugController = new BugController();
                    Boolean updated = bugController.UpdateBugStatusFixing(bug);
                    if (updated)
                    {
                        MessageBox.Show("Bug status updated!");
                        this.Dispose();
                    }
                    else
                    {
                        MessageBox.Show("Unable to update bug status!");
                    }
                }
                //if status is "Fixed"
                else
                {
                    if (markAs == "Fixed" && string.IsNullOrEmpty(month) || string.IsNullOrEmpty(date) ||
                        string.IsNullOrEmpty(year))
                    {
                        MessageBox.Show("Please specify complete bug fixed date!");
                    }
                    else
                    {
                        string concatenatedDate = month + "-" + date + "-" + year;
                        Bug bug = new Bug();
                        bug.FixerId = userId;
                        bug.BugId = bugId;
                        bug.Status = markAs;
                        bug.FixDate = concatenatedDate;
                        BugController bugController = new BugController();
                        Boolean updated = bugController.UpdateBugStatusFixed(bug);
                        if (updated)
                        {
                            MessageBox.Show("Bug status updated!");
                            this.Dispose();
                        }
                        else
                        {
                            MessageBox.Show("Unable to update bug status!");
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("Please specify mark as type!");
            }
        }

        /// <summary>
        /// event triggered rich text box text change
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_bugDescription_TextChanged(object sender, EventArgs e)
        {
            // getting keywords/functions
            string keywords = @"\b(public|private|partial|static|namespace|class|using
                            |void|foreach|in|int|string|this|new|try|catch|if|else|)\b";
            MatchCollection keywordMatches = Regex.Matches(txt_bugDescription.Text, keywords);

            //getting types/classes from the text 
            string types = @"\b(Console|MessageBox)\b";
            MatchCollection typeMatches = Regex.Matches(txt_bugDescription.Text, types);

            //getting comments (inline or multiline)
            string comments = @"(\/\/.+?$|\/\*.+?\*\/)";
            MatchCollection commentMatches = Regex.Matches(txt_bugDescription.Text, comments, RegexOptions.Multiline);

            //getting strings
            string strings = "\".+?\"";
            MatchCollection stringMatches = Regex.Matches(txt_bugDescription.Text, strings);

            // saving the original caret position + forecolor
            int originalIndex = txt_bugDescription.SelectionStart;
            int originalLength = txt_bugDescription.SelectionLength;
            Color originalColor = Color.Black;

            // MANDATORY - focuses a label before highlighting (avoids blinking)
            lbl_bugDescription.Focus();

            // removes any previous highlighting (so modified words won't remain highlighted)
            txt_bugDescription.SelectionStart = 0;
            txt_bugDescription.SelectionLength = txt_bugDescription.Text.Length;
            txt_bugDescription.SelectionColor = originalColor;

            // scanning...
            foreach (Match match in keywordMatches)
            {
                txt_bugDescription.SelectionStart = match.Index;
                txt_bugDescription.SelectionLength = match.Length;
                txt_bugDescription.SelectionColor = Color.Blue;
            }

            foreach (Match match in typeMatches)
            {
                txt_bugDescription.SelectionStart = match.Index;
                txt_bugDescription.SelectionLength = match.Length;
                txt_bugDescription.SelectionColor = Color.DarkCyan;
            }

            foreach (Match match in commentMatches)
            {
                txt_bugDescription.SelectionStart = match.Index;
                txt_bugDescription.SelectionLength = match.Length;
                txt_bugDescription.SelectionColor = Color.Green;
            }

            foreach (Match match in stringMatches)
            {
                txt_bugDescription.SelectionStart = match.Index;
                txt_bugDescription.SelectionLength = match.Length;
                txt_bugDescription.SelectionColor = Color.Brown;
            }

            // restoring the original colors, for further writing
            txt_bugDescription.SelectionStart = originalIndex;
            txt_bugDescription.SelectionLength = originalLength;
            txt_bugDescription.SelectionColor = originalColor;

            // giving back the focus
            txt_bugDescription.Focus();
        }
    }
}
