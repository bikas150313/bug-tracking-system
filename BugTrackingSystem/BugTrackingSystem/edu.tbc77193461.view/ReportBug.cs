﻿using BugTrackingSystem.edu.tbc77193461.controller;
using BugTrackingSystem.edu.tbc77193461.model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BugTrackingSystem.edu.tbc77193461.view
{
    public partial class ReportBug : Form
    {

        /// <summary>
        /// declaring required variables
        /// </summary>
        private string fileName;
        private int userId;

        public ReportBug(int userId)
        {
            this.userId = userId;
            InitializeComponent();
        }

        /// <summary>
        /// event triggered on cancel button click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_cancel_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        /// <summary>
        /// event triggered on browse button click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_browse_Click(object sender, EventArgs e)
        {
            try
            {
                //opens file dialog
                OpenFileDialog fileDialog = new OpenFileDialog();
                //filtering image type
                fileDialog.Filter = "Choose Image(*.jpg; *.png; *.bmp)|*.jpg; *.png; *.bmp";
                if (fileDialog.ShowDialog() == DialogResult.OK)
                {
                    //capturing file name
                    fileName = fileDialog.FileName;
                    lbl_fileName.Text = fileName;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// event triggered on report button click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_report_Click(object sender, EventArgs e)
        {
            string bugDescription = txt_bugDescription.Text;
            string project = txt_project.Text;
            string className = txt_class.Text;
            string method = txt_method.Text;
            string lineNo = txt_lineNumber.Text;
            string codeAuthor = txt_codeAuthor.Text;
            string month = combo_month.Text;
            string date = combo_date.Text;
            string year = combo_year.Text;
            //checking validation
            if (!string.IsNullOrEmpty(bugDescription) && !string.IsNullOrEmpty(project)
                && !string.IsNullOrEmpty(month) && !string.IsNullOrEmpty(date) &&
                !string.IsNullOrEmpty(year) && !string.IsNullOrEmpty(fileName))
            {
                string concatenatedDate = month + "-" + date + "-" + year;
                Bug bug = new Bug();
                bug.UserId = userId;
                bug.BugDescription = bugDescription;
                bug.Project = project;
                bug.ClassName = className;
                bug.Method = method;
                bug.LineNumber = lineNo;
                bug.Author = codeAuthor;
                bug.Date = concatenatedDate;
                bug.FileName = fileName;
                bug.Status = "Active";
                BugController bugController = new BugController();
                Boolean reported = bugController.ReportBug(bug);
                if (reported)
                {
                    MessageBox.Show("Bug successfully reported!");
                    txt_bugDescription.Text = "";
                    txt_project.Text = "";
                    txt_class.Text = "";
                    txt_method.Text = "";
                    txt_lineNumber.Text = "";
                    txt_codeAuthor.Text = "";
                    combo_month.Text = "";
                    combo_date.Text = "";
                    combo_year.Text = "";
                    lbl_fileName.Text = "";
                    txt_bugDescription.Focus();
                }
                else
                {
                    MessageBox.Show("Unable to report bug!");
                }
            }
            else
            {
                MessageBox.Show("Fields marked as Asterisk (*) are mandatory!");
            }
        }

        /// <summary>
        /// event triggered on richtextbox text change
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_bugDescription_TextChanged(object sender, EventArgs e)
        {
            // getting keywords/functions
            string keywords = @"\b(public|private|partial|static|namespace|class|using
                            |void|foreach|in|int|string|this|new|try|catch|if|else|)\b";
            MatchCollection keywordMatches = Regex.Matches(txt_bugDescription.Text, keywords);

            //getting types/classes from the text 
            string types = @"\b(Console|MessageBox)\b";
            MatchCollection typeMatches = Regex.Matches(txt_bugDescription.Text, types);

            //getting comments (inline or multiline)
            string comments = @"(\/\/.+?$|\/\*.+?\*\/)";
            MatchCollection commentMatches = Regex.Matches(txt_bugDescription.Text, comments, RegexOptions.Multiline);

            //getting strings
            string strings = "\".+?\"";
            MatchCollection stringMatches = Regex.Matches(txt_bugDescription.Text, strings);

            // saving the original caret position + forecolor
            int originalIndex = txt_bugDescription.SelectionStart;
            int originalLength = txt_bugDescription.SelectionLength;
            Color originalColor = Color.Black;

            // MANDATORY - focuses a label before highlighting (avoids blinking)
            lbl_bugDescription.Focus();

            // removes any previous highlighting (so modified words won't remain highlighted)
            txt_bugDescription.SelectionStart = 0;
            txt_bugDescription.SelectionLength = txt_bugDescription.Text.Length;
            txt_bugDescription.SelectionColor = originalColor;

            // scanning...
            foreach (Match match in keywordMatches)
            {
                txt_bugDescription.SelectionStart = match.Index;
                txt_bugDescription.SelectionLength = match.Length;
                txt_bugDescription.SelectionColor = Color.Blue;
            }

            foreach (Match match in typeMatches)
            {
                txt_bugDescription.SelectionStart = match.Index;
                txt_bugDescription.SelectionLength = match.Length;
                txt_bugDescription.SelectionColor = Color.DarkCyan;
            }

            foreach (Match match in commentMatches)
            {
                txt_bugDescription.SelectionStart = match.Index;
                txt_bugDescription.SelectionLength = match.Length;
                txt_bugDescription.SelectionColor = Color.Green;
            }

            foreach (Match match in stringMatches)
            {
                txt_bugDescription.SelectionStart = match.Index;
                txt_bugDescription.SelectionLength = match.Length;
                txt_bugDescription.SelectionColor = Color.Brown;
            }

            // restoring the original colors, for further writing
            txt_bugDescription.SelectionStart = originalIndex;
            txt_bugDescription.SelectionLength = originalLength;
            txt_bugDescription.SelectionColor = originalColor;

            // giving back the focus
            txt_bugDescription.Focus();
        }
    }
}
