﻿using BugTrackingSystem.edu.tbc77193461.controller;
using BugTrackingSystem.edu.tbc77193461.model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BugTrackingSystem.edu.tbc77193461.view
{
    public partial class EditUser : Form
    {
        
        /// <summary>
        /// declaring required variable
        /// </summary>
        private int userId;

        public EditUser(int userId, string username, string fullName, string address, 
            string contactNo, string email, string role)
        {
            InitializeComponent();
            this.userId = userId;
            //initializing form component
            txt_userName.Text = username;
            txt_fullName.Text = fullName;
            txt_address.Text = address;
            txt_contactNo.Text = contactNo;
            txt_email.Text = email;
            combo_role.Text = role;
        }

        /// <summary>
        /// event triggered on cancel button click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_cancel_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        /// <summary>
        /// event triggered on update button click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_update_Click(object sender, EventArgs e)
        {
            string fullName = txt_fullName.Text;
            string username = txt_userName.Text;
            string address = txt_address.Text;
            string contactNo = txt_contactNo.Text;
            string email = txt_email.Text;
            string role = combo_role.Text;
            if (!string.IsNullOrEmpty(fullName) && !string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(address)
                && !string.IsNullOrEmpty(contactNo) && !string.IsNullOrEmpty(email) && !string.IsNullOrEmpty(role))
            {
                User user = new User();
                user.UserId = userId;
                user.FullName = fullName;
                user.Username = username;
                user.Address = address;
                user.ContactNo = contactNo;
                user.Email = email;
                user.Role = role;
                UserController userController = new UserController();
                Boolean updated = userController.UpdateUser(user);
                if (updated)
                {
                    MessageBox.Show("Profile Updated!");
                    Dispose();
                }
                else
                {
                    MessageBox.Show("Unable to update profile!");
                }
            }
            else
            {
                MessageBox.Show("Please input all the fields!");
            }
        }
    }
}
