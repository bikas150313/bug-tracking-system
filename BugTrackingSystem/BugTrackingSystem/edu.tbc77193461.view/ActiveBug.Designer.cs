﻿namespace BugTrackingSystem.edu.tbc77193461.view
{
    partial class ActiveBug
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ActiveBug));
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.pan_allUsers = new System.Windows.Forms.Panel();
            this.btn_refresh = new System.Windows.Forms.Button();
            this.btn_viewAndUpdateDetails = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            this.pan_allUsers.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.Location = new System.Drawing.Point(0, 70);
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.ReadOnly = true;
            this.dataGridView.Size = new System.Drawing.Size(744, 438);
            this.dataGridView.TabIndex = 2;
            // 
            // pan_allUsers
            // 
            this.pan_allUsers.BackColor = System.Drawing.Color.Black;
            this.pan_allUsers.Controls.Add(this.btn_refresh);
            this.pan_allUsers.Controls.Add(this.btn_viewAndUpdateDetails);
            this.pan_allUsers.Location = new System.Drawing.Point(0, 0);
            this.pan_allUsers.Name = "pan_allUsers";
            this.pan_allUsers.Size = new System.Drawing.Size(744, 64);
            this.pan_allUsers.TabIndex = 3;
            // 
            // btn_refresh
            // 
            this.btn_refresh.Font = new System.Drawing.Font("Algerian", 15.75F);
            this.btn_refresh.ForeColor = System.Drawing.Color.Blue;
            this.btn_refresh.Image = ((System.Drawing.Image)(resources.GetObject("btn_refresh.Image")));
            this.btn_refresh.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_refresh.Location = new System.Drawing.Point(12, 12);
            this.btn_refresh.Name = "btn_refresh";
            this.btn_refresh.Size = new System.Drawing.Size(133, 36);
            this.btn_refresh.TabIndex = 2;
            this.btn_refresh.Text = "Refresh";
            this.btn_refresh.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_refresh.UseVisualStyleBackColor = true;
            this.btn_refresh.Click += new System.EventHandler(this.btn_refresh_Click);
            // 
            // btn_viewAndUpdateDetails
            // 
            this.btn_viewAndUpdateDetails.Font = new System.Drawing.Font("Algerian", 15.75F);
            this.btn_viewAndUpdateDetails.ForeColor = System.Drawing.Color.Green;
            this.btn_viewAndUpdateDetails.Image = ((System.Drawing.Image)(resources.GetObject("btn_viewAndUpdateDetails.Image")));
            this.btn_viewAndUpdateDetails.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_viewAndUpdateDetails.Location = new System.Drawing.Point(424, 12);
            this.btn_viewAndUpdateDetails.Name = "btn_viewAndUpdateDetails";
            this.btn_viewAndUpdateDetails.Size = new System.Drawing.Size(308, 36);
            this.btn_viewAndUpdateDetails.TabIndex = 0;
            this.btn_viewAndUpdateDetails.Text = "View and Update Details";
            this.btn_viewAndUpdateDetails.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_viewAndUpdateDetails.UseVisualStyleBackColor = true;
            this.btn_viewAndUpdateDetails.Click += new System.EventHandler(this.btn_viewAndUpdateDetails_Click);
            // 
            // ActiveBug
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(744, 508);
            this.Controls.Add(this.pan_allUsers);
            this.Controls.Add(this.dataGridView);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ActiveBug";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Active Bugs";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            this.pan_allUsers.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView;
        private System.Windows.Forms.Panel pan_allUsers;
        private System.Windows.Forms.Button btn_refresh;
        private System.Windows.Forms.Button btn_viewAndUpdateDetails;
    }
}