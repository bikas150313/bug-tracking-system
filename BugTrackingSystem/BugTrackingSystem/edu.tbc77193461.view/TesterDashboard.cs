﻿using BugTrackingSystem.edu.tbc77193461.controller;
using BugTrackingSystem.edu.tbc77193461.model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BugTrackingSystem.edu.tbc77193461.view
{
    public partial class TesterDashboard : Form
    {

        /// <summary>
        /// declaring required variables
        /// </summary>
        private string username;
        private int userId;

        public TesterDashboard()
        {
            InitializeComponent();
        }

        public TesterDashboard(string username, int userId)
        {
            this.username = username;
            this.userId = userId;
            this.WindowState = FormWindowState.Maximized;
            InitializeComponent();
        }

        /// <summary>
        /// event triggered on logout menu click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void menuItem_logout_Click(object sender, EventArgs e)
        {
            this.Hide();
            Login login = new Login();
            login.ShowDialog();
            this.Close();
        }

        /// <summary>
        /// event triggered on change password menu click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void menuItem_changePassword_Click(object sender, EventArgs e)
        {
            ChangePassword changePassword = new ChangePassword(username);
            changePassword.ShowDialog();
        }

        /// <summary>
        /// event triggered on view profile menu click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void menuItem_viewProfile_Click(object sender, EventArgs e)
        {
            User user = new User();
            user.UserId = userId;
            UserController userController = new UserController();
            userController.RetrieveUserData(user);
            string fullName = userController.fullName;
            string username = userController.username;
            string address = userController.address;
            string contactNo = userController.contactNo;
            string email = userController.email;
            string role = userController.role;
            UserProfile profile = new UserProfile(fullName, username, address, contactNo,
                email, role);
            profile.ShowDialog();
        }

        /// <summary>
        /// event triggered on report bug menu click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void menuItem_reportBug_Click(object sender, EventArgs e)
        {
            ReportBug reportBug = new ReportBug(userId);
            reportBug.ShowDialog();
        }

        /// <summary>
        /// event triggered on my bug menu click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void menuItem_myBug_Click(object sender, EventArgs e)
        {
            MyBug myBug = new MyBug(userId);
            myBug.ShowDialog();
        }

        /// <summary>
        /// opens bug detail form
        /// </summary>
        /// <param name="bugId"></param>
        /// <param name="bugDescription"></param>
        /// <param name="project"></param>
        /// <param name="className"></param>
        /// <param name="method"></param>
        /// <param name="lineNo"></param>
        /// <param name="author"></param>
        /// <param name="memoryStream"></param>
        /// <param name="reportDate"></param>
        /// <param name="status"></param>
        /// <param name="fixDate"></param>
        /// <param name="fixer"></param>
        public void OpenBugDetailFormWithFixer(int bugId, string bugDescription, string project, string className, string method,
            string lineNo, string author, MemoryStream memoryStream, string reportDate, string status, string fixDate, 
            string fixer)
        {
            BugDetail bugDetail = new BugDetail(bugId, bugDescription, project, className, method, lineNo, author,
                memoryStream, reportDate, status, fixDate, fixer);
            bugDetail.ShowDialog();
        }

        /// <summary>
        /// open bug detail form
        /// </summary>
        /// <param name="bugId"></param>
        /// <param name="bugDescription"></param>
        /// <param name="project"></param>
        /// <param name="className"></param>
        /// <param name="method"></param>
        /// <param name="lineNo"></param>
        /// <param name="author"></param>
        /// <param name="memoryStream"></param>
        /// <param name="reportDate"></param>
        /// <param name="status"></param>
        /// <param name="fixDate"></param>
        /// <param name="fixer"></param>
        /// <param name="markedAs"></param>
        public void OpenBugDetailFormWithoutFixer(int bugId, string bugDescription, string project, string className, string method,
            string lineNo, string author, MemoryStream memoryStream, string reportDate, string status, string fixDate,
            string fixer, string markedAs)
        {
            BugDetail bugDetail = new BugDetail(bugId, bugDescription, project, className, method, lineNo, author,
                memoryStream, reportDate, status, fixDate, fixer, markedAs);
            bugDetail.ShowDialog();
        }

        /// <summary>
        /// event triggered on connect to bitbucket menu click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void menuItem_connectToBitbucket_Click(object sender, EventArgs e)
        {
            //url to open bitbucket
            string url = "https://bitbucket.org/account/signin/";
            //executing url
            System.Diagnostics.Process.Start(url);
        }

        /// <summary>
        /// event triggered on about menu click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void menuItem_about_Click(object sender, EventArgs e)
        {
            About about = new About();
            about.ShowDialog();
        }
    }
}
